<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('national_id')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('default_password');
            $table->string('image')->nullable();
            $table->date('dob'); // date of birth
           
            $table->integer('country_id')->unsigned(); // relation between country and city
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('user_types')->onDelete('cascade');

            $table->char('gender');
            $table->string('mobile_number');
            $table->text('address');

            $table->integer('supervisor_id')->unsigned()->nullable();
            $table->foreign('supervisor_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('job_title_id')->unsigned()->nullable();
            $table->foreign('job_title_id')->references('id')->on('job_titles')->onDelete('cascade');


            $table->date('start_hire_date');
            $table->char('rank'); // team Leader - senior - middle - junior 
            $table->integer('total_vacation_days')->default(21);

            $table->integer('years_of_experience');
            $table->string('social_insurance_Number');

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
