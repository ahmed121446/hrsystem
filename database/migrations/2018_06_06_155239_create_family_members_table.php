<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_members', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('social_status_id')->unsigned();
            $table->foreign('social_status_id')->references('id')->on('social_statuses')->onDelete('cascade');

            $table->string('name_en');
            $table->string('name_ar');

            $table->string('email')->nullable();
            $table->integer('national_id');
            $table->char('gender'); // M - F
            $table->date('dob')->nullable(); // date of birth
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_members');
    }
}
