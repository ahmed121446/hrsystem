<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\UserType;
use App\City;
use App\Country;
use App\User;
use App\JobTitle;
use Illuminate\Support\Facades\Storage;


use Illuminate\Filesystem\Filesystem;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();

        $file = new Filesystem;
        $file->cleanDirectory('storage/app/public/images/users');

        $boy = 'app/staticImages/boy.png';
        $girl = 'app/staticImages/girl.png';

        $boy_content = file_get_contents($boy);
        $girl_content = file_get_contents($girl);

        Storage::put('public/images/users/boy.png', $boy_content);
        Storage::put('public/images/users/girl.png', $girl_content);


        $rank = ['J','M','S','T'];
        $k = array_rand($rank);
        $rank = $rank[$k];

        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        DB::table('users')->insert([
            // supervisor_id
            'name_en' => 'Ahmed Magdy',
            'name_ar' => 'احمد مجدي',
            'national_id' => "11223344556677",
            'email' => 'trmdy@hotmail.com',
            'password' => bcrypt('111111111'),
            'default_password' => bcrypt('111111111'),
            'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
            'image' => $this->getImage(),
            'country_id' => Country::all()->random()->id,
            'type_id' => 3, // super admin
            'gender' => 'M',
            'mobile_number' => $faker->phoneNumber,
            'start_hire_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
            'rank' => $rank,
            'job_title_id' =>JobTitle::all()->random()->id ,
            'social_insurance_Number' => $faker->numberBetween($min = 1000, $max = 9000),
            'years_of_experience' => $faker->numberBetween($min = 0, $max = 20),
            'address'=> $faker->address,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);


        // for ($i=0; $i < 35; $i++) { 
        //     DB::table('users')->insert([
        //         'name_en' => $faker->name,
        //         'name_ar' => $faker_ar->name,
        //         'national_id' => $faker->creditCardNumber,
        //         'email' => $faker->email,
        //         'password' => bcrypt('111111111'),
        //         'default_password' => bcrypt('111111111'),
        //         'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //         'image' => $this->getImage(),
        //         'country_id' => Country::all()->random()->id,
        //         'type_id' => rand(0,1) ? 1 : 2 ,
        //         'gender' => 'M',
        //         'mobile_number' => $faker->phoneNumber,
        //         'start_hire_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        //         'rank' => $rank,
        //         'job_title_id' =>JobTitle::all()->random()->id ,
        //         'supervisor_id' => (rand(0,1)) ? User::all()->random()->id : null,
        //         'social_insurance_Number' => $faker->numberBetween($min = 1000, $max = 9000),
        //         'years_of_experience' => $faker->numberBetween($min = 0, $max = 20),
        //         'address'=> $faker->address,
        //         'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //         'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     ]);
        // }
    }


    protected function getImage(){
        $faker = Faker::create();
        $img_name = $faker->image('public/storage/images/users/', 300, 300, null, false);
        if(!$img_name){
            $img = Image::canvas(300, 300, '#f44e42');
            $path = storage_path('app/public/images/users/');
            $img_name = uniqid().'.png';
            $img->save($path.''.$img_name);
        }
        return $img_name;
    }
}
