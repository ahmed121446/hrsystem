<?php

use Illuminate\Database\Seeder;

use App\Country;


class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->truncate();
        $path = 'database/seeds/include/countries.sql';
        \DB::unprepared( file_get_contents($path) );
        $this->command->info('Country table seeded!');
    }
}
