<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Department;

class JobTitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('job_titles')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        for ($i=0; $i < 10; $i++) { 
            DB::table('job_titles')->insert([
                'name_en' => $faker->name,
                'name_ar' => $faker_ar->name,
                'department_id' => Department::all()->random()->id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
