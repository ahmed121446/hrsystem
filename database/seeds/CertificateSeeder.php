<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\User;

class CertificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('certificates')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');

        for($i = 0 ; $i < 4 ; $i++) { 
            DB::table('certificates')->insert([
                'user_id' => User::all()->random()->id,
                'name_en' => $faker->name,
                'name_ar' => $faker_ar->name,
                'date' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
