<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

use App\SocialStatus;

class FamilyMembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('family_members')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');
        $social_statuses = SocialStatus::where('is_bread_winner', 1 )->get();
        $counter = 1;
        foreach ($social_statuses as $social_state ) {
            for ($i=0; $i < $faker->randomDigitNotNull; $i++) { 
                DB::table('family_members')->insert([
                    'social_status_id' => $social_state->id,
                    'name_en' => $faker->name,
                    'name_ar' => $faker_ar->name,
                    'email' => $faker->email,
                    'national_id' => '12121212'.$counter,
                    'gender' => rand(0,1)?'M':'F',
                    'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);  
                $counter++;
            }
        }
    }
}
