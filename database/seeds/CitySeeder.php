<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Country;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('cities')->truncate();
        $faker = Faker::create();
        $faker_ar = Faker::create('ar_SA');
        for($i = 0 ; $i < 150 ; $i++) { 
            DB::table('cities')->insert([
                'name_en' => $faker->city,
                'name_ar' => $faker_ar->city,
                'country_id' => Country::all()->random()->id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
