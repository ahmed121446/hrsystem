<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Contract;
use App\User;

class ContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('contracts')->truncate();
        $faker = Faker::create();


        for($i = 0 ; $i < 2 ; $i++) { 

            $monthly_salary = $faker->numberBetween($min = 8000, $max = 15000);
            $total_annual = $monthly_salary * 12;

            $user_id =  User::all()->random()->id;
            $is_exist = Contract::where('user_id',$user_id)->where('is_active',1)->first();
            if ($is_exist) {
                $i--;
                continue;
            }

            $year = $faker->numberBetween($min = 10, $max = 18);
            
            $now        = Carbon::now()->format('Y-m-d');
            $start_date = $faker->date($format = '20'.$year.'-m-d', $max = 'now');
            $end_date   = $faker->date($format = '20'.($year+1).'-m-d', $max = 'now');

            if ( $now < $end_date) {
                $active = true;
            }else{
                $active = false;
            }
            DB::table('contracts')->insert([
                'start_date' => $start_date,
                'end_date' => $end_date,

                'is_active' => $active,

                'total_annual' => $total_annual,
                'monthly_salary' => $monthly_salary,

                'currancy'=>'$',
                'user_id' => User::all()->random()->id,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
