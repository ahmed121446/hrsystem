<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Carbon\Carbon;

class Salary extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'date',
        'incentive',
        'deduction',
        'other',
        'allowance',
        'tax',
        'loan',
        'net',
        'total',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * relation between ( Contract::class  -  Salary::class)
     * (1-M) Relationship  [ contract - salaries ]
     * @return void
     */
    public function contract(){
        return $this->belongsTo(Contract::class,'contract_id');
    }

     /**
     * scopefilterEmployee function
     * return employees by name 
     * @param [type] $query
     * @return void
     */
    public function scopefilterEmployee($query,$employee_name){
        return $query->whereHas('contract',function($new_query) use ($employee_name){
            return $new_query->whereHas('user',function($q) use ($employee_name){
                $q->filterName($employee_name);
            });
        });
    }


    public function scopeFilter($query , $filtermonth , $filteryear)
    {
    	if ( $month = $filtermonth ) {
           $query->whereMonth('date',Carbon::parse($month)->month);
        }
        if ( $year = $filteryear ) {
            $query->whereYear('date',$year);
        }
    }

    public static function Schedule()
    {
    	return static::selectRaw('year(date) year, monthname(date) month, count(*) count')
                    ->groupBy('year','month')
                    ->orderByRaw(' min(date) desc')
                    ->get()
                    ->toArray();
    }


    public function scopeSalaryExist($query ,$date , $contract_id)
    {
    	return $query->where('contract_id',$contract_id)
    				->where('date',$date); 
    }

}
