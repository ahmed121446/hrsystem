<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FamilyMember extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'social_status_id',
        'name_en',
        'name_ar',
        'email',
        'national_id',
        'gender',
        'dob',
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * relation between ( SocialStatus::class  -  FamilyMember::class)
     * (1-M) Relationship  [ socialStatus - familyMembers ]
     * @return void
     */
    public function socialStatus(){
        return $this->belongsTo(SocialStatus::class,'social_status_id');
    }

}
