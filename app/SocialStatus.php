<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SocialStatus extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'is_bread_winner',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];


    /**
     * relation between ( SocialStatus::class  -  User::class)
     * (1-1) Relationship  [ user - socialStatus ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    
    /**
     * relation between ( SocialStatus::class  -  FamilyMember::class)
     * (1-M) Relationship  [ socialStatus - familyMembers ]
     * @return void
     */
    public function familyMembers(){
        return $this->hasMany(FamilyMember::class,'social_status_id');
    }

}
