<?php

namespace App\Repositories\Backend;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class EmployeeRepository {

    public function storeUserImage($request){
        $image = '';
        if ($request->has('image') ) {
            $image = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs(
                'public/images/users/', $image
            );
        }else{
            if ($request->get('gender') == 'M') {
                $image = 'boy.png';
            }else {
                $image = 'girl.png';
            }
        }

        return $image;
    }

    public function ageValidation($request){
        $start_hire_date = $request->get('start_hire_date');
        $dob = $request->get('dob');
        $start_hire_date = new Carbon($start_hire_date);
        $dob = new Carbon($dob);
        $diff = $dob->diff($start_hire_date)->days;

        $valid_days = 5475; // 15 years
        if($diff < $valid_days){
            \Log::info('DATE DIFF > 15',['dob'=>$dob , 'start_hire_date'=>$start_hire_date ,'diff'=>$diff]);
            return false;
        }
        \Log::info('DATE DIFF > 15',['dob'=>$dob , 'start_hire_date'=>$start_hire_date ,'diff'=>$diff]);
        return true;
    }
}