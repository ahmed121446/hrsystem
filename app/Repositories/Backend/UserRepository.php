<?php

namespace App\Repositories\Backend;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Storage;


class UserRepository {

    public function storeUserImage($request){
        $image = '';
        if ($request->has('image') ) {
            $image = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs(
                'public/images/users/', $image
            );
        }else{
            if ($request->get('gender') == 'M') {
                $image = 'boy.png';
            }else {
                $image = 'girl.png';
            }
        }

        return $image;
    }

    public function getUserImage(User $user){
        if($exists = Storage::exists('public/images/users/'.$user->image)){
            $image = Storage::get('public/images/users/'.$user->image);
            return $image ;
        }
        return false;
    }
}