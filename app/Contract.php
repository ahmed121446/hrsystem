<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Contract extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'start_date',
        'end_date',
        'is_active',
        'total_annual',
        'monthly_salary',
        'currancy',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    /**
     * relation between ( Contract::class  -  User::class)
     * (1-M) Relationship  [ user - contracts ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * relation between ( Contract::class  -  Salary::class)
     * (1-M) Relationship  [ contract - salaries ]
     * @return void
     */
    public function salaries(){
        return $this->hasMany(Salary::class,'contract_id');
    }

    /**
     * scopeFilterActive function
     * return active contracts
     * @param [type] $query
     * @return void
     */
    public function scopeFilterActive($query){
        return $query->where('is_active',true);
    }

    /**
     * scopeFilterActive function
     * return active contracts
     * @param [type] $query
     * @return void
     */
    public function scopefilterEmployee($query,$employee_name){
        return $query->whereHas('user',function($new_query) use ($employee_name){
            $new_query->filterName($employee_name);
        });
    }

    /**
     * scopeFilter function
     * filter by date and month from request
     * @param [type] $query
     * @param [type] $filtermonth
     * @param [type] $filteryear
     * @return void
     */
    public function scopeFilter($query , $filtermonth , $filteryear)
    {
    	if ( $month = $filtermonth ) {
           $query->whereMonth('start_date',Carbon::parse($month)->month);
        }
        if ( $year = $filteryear ) {
            $query->whereYear('start_date',$year);
        }
    }

    /**
     * Schedule function
     * return years and months groupBy
     * @return void
     */
    public static function Schedule()
    {
    	return static::selectRaw('year(start_date) year, monthname(start_date) month, count(*) count')
                    ->groupBy('year','month')
                    ->orderByRaw(' min(start_date) desc')
                    ->get()
                    ->toArray();
    }


    /**
     * scopeContractExist function
     *
     * @param [type] $query
     * @param [type] $startDate
     * @param [type] $endDate
     * @param [type] $employee_id
     * @return void
     */
    public function scopeContractExist($query ,$startDate,$endDate , $user_id)
    {
        return $query->where('user_id',$user_id)
                    ->whereBetween('start_date', array($startDate, $endDate))
                    ->whereBetween('end_date', array($startDate, $endDate));
    }

    /**
     * scopeContractActive function
     *
     * @param [type] $query
     * @param [type] $employee_id
     * @return void
     */
    public function scopeContractActive($query , $user_id)
    {
        return $query->where('user_id',$user_id)
                    ->where('is_active', 1);
    }
}
