<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Department extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name_en',
        'name_ar',
        'description_en',
        'description_ar',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at',
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }

    /**
     * Get description by the specified language
     * @return [String] name
     */
    public function getDescriptionAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->description_ar : $this->description_en;
        return $name;
    }


    /**
     * relation between ( Department::class  -  JobTitle::class)
     *
     * @return void
     */
    public function jobTitles(){
        return $this->hasMany(JobTitle::class,'department_id');
    }


    public function scopeFilterName($query , $name){
        if ($name) {
            if (\App::getLocale() == 'ar') {
                return  $query->where('name_ar','like',"%{$name}%");
            }else if(\App::getLocale() == 'en'){
                return  $query->where('name_en','like',"%{$name}%");
            }else{
                return  $query->where('name_en','like',"%{$name}%");
            }
        }
    }

}
