<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SkillLevel extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'level_code',
        'description_en',
        'description_ar',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * relation between ( SkillLevel::class  -  UserSkill::class)
     * (1-M) Relationship  [ skillLevel - userSkills ]
     * @return void
     */
    public function userSkills(){
        return $this->hasMany(UserSkill::class,'skill_level_id');
    }

}
