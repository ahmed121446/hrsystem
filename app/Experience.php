<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'start_date',
        'end_date',
        'role_en',
        'role_ar',
        'description_en',
        'description_ar',
        'where',
    ];

    /**
     * Get role by the specified language
     * @return [String] role
     */
    public function getRoleAttribute()
    {
        $role = app('App\Services\MultiLang')->lang == '_ar' ? $this->role_ar : $this->role_en;
        return $role;
    }

    /**
     * Get description by the specified language
     * @return [String] role
     */
    public function getDescriptionAttribute()
    {
        $description = app('App\Services\MultiLang')->lang == '_ar' ? $this->description_ar : $this->description_en;
        return $description;
    }


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * relation between ( Experience::class  -  User::class)
     * (1-M) Relationship  [ user - experiences ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
