<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class JobTitle extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name_en',
        'name_ar',
        'department_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at',
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }

    /**
     * relation between ( User::class  -  JobTitle::class)
     *
     * @return void
     */
    public function users(){
        return $this->hasMany(User::class,'job_title_id');
    }

    /**
      * relation between ( Department::class  -  JobTitle::class)
      * @return void
      */
      public function department(){
        return $this->belongsTo(Department::class);
    }


    public function scopeFilterName($query , $name){
        if ($name) {
            if (\App::getLocale() == 'ar') {
                return  $query->where('name_ar','like',"%{$name}%");
            }else if(\App::getLocale() == 'en'){
                return  $query->where('name_en','like',"%{$name}%");
            }else{
                return  $query->where('name_en','like',"%{$name}%");
            }
        }
    }
    public function scopeFilterDepartment($query , $name){
        if ($name) {  
            return $query->whereHas('department',function($new_query) use ($name){
                $new_query->filterName($name);
            });
        }
    }

}
