<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewEmployeeNotification extends Notification
{
    use Queueable;
    public $supervisor;
    public $employee;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($supervisor,$employee)
    {
        $this->supervisor=$supervisor;
        $this->employee=$employee;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown(
            'mail.supervisor',
            [
                'supervisor'=>$this->supervisor,
                'employee'=>$this->employee,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = [
            'body'=>[
                'en'=>[
                    'head'=> trans('notification/newEmployee.head',[],'en')  ,
                    'content'=> trans('notification/newEmployee.content',[],'en'),
                ],
                'ar'=>[
                    'head'=>trans('notification/newEmployee.head',[],'ar'),
                    'content'=>trans('notification/newEmployee.content',[],'ar'),
                ],
                'id'=> $this->employee->id,
                'type'=>'NewEmployee',
            ]
        ];
        \Log::info($data);
        return $data;
    }
}
