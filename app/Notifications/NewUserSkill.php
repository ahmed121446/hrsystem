<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserSkill extends Notification implements ShouldQueue
{
    use Queueable;
    
    public $user;
    public $skill;
    public $level;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$skill,$level)
    {
        $this->user = $user;
        $this->skill = $skill;
        $this->level = $level;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown(
            'mail.user_skill',
            [
                'user'=>$this->user,
                'level'=>$this->level,
                'skill'=>$this->skill,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = [
            'body'=>[
                'en'=>[
                    'head'=> trans('notification/newSkill.head',[],'en')  ,
                    'content'=> trans('notification/newSkill.content',[],'en'),
                ],
                'ar'=>[
                    'head'=>trans('notification/newSkill.head',[],'ar'),
                    'content'=>trans('notification/newSkill.content',[],'ar'),
                ],
                'id'=> $this->skill->id,
                'type'=>'NewSkill',
            ]
        ];
        \Log::info($data);
        return $data;
    }
}
