<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewContract extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $contract;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$contract)
    {
        $this->user = $user;
        $this->contract = $contract;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown(
            'mail.contract',
            [
                'user'=>$this->user,
                'contract'=>$this->contract,
            ]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data = [
            'body'=>[
                'en'=>[
                    'head'=> trans('notification/newContract.head',[],'en')  ,
                    'content'=> trans('notification/newContract.content',[],'en'),
                ],
                'ar'=>[
                    'head'=>trans('notification/newContract.head',[],'ar'),
                    'content'=>trans('notification/newContract.content',[],'ar'),
                ],
                'id'=> $this->contract->id,
                'type'=>'NewContract',
            ]
        ];
        \Log::info($data);
        return $data;
    }
}
