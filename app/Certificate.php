<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name_en',
        'name_ar',
        'location',
        'date',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }


    /**
     * relation between ( Certificate::class  -  User::class)
     * (1-M) Relationship  [ user - certificate ]
     * @return void
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
