<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name_en',
        'name_ar',
        'iso_code',
        'country_code',
    ];


    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at','deleted_at'
    ];

    /**
     * Get name by the specified language
     * @return [String] name
     */
    public function getNameAttribute()
    {
        $name = app('App\Services\MultiLang')->lang == '_ar' ? $this->name_ar : $this->name_en;
        return $name;
    }

    // /**
    //  * relation between ( Country::class  -  City::class)
    //  * (1-M) Relationship  [ country - cities ]
    //  * @return void
    //  */
    // public function cities(){
    //     return $this->hasMany(City::class,'country_id');
    // }

    /**
     * relation between ( Country::class  -  User::class)
     * (1-M) Relationship  [ country - users ]
     * @return void
     */
    public function users(){
        return $this->hasMany(User::class,'country_id');
    }

}
