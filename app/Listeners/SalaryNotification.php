<?php

namespace App\Listeners;

use App\Events\NewSalary;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Notifications\NewSalary as NewSalaryNotification;

class SalaryNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSalary  $event
     * @return void
     */
    public function handle(NewSalary $event)
    {
        $user = $event->user;
        $salary = $event->salary;
        //send email and notification
        $user->notify(new NewSalaryNotification($user,$salary));
    }
}
