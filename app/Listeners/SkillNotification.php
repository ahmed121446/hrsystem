<?php

namespace App\Listeners;

use App\Events\AddedSkill;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Notifications\NewUserSkill as NewUserSkillNotification;


class SkillNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddedSkill  $event
     * @return void
     */
    public function handle(AddedSkill $event)
    {
        $user = $event->user;
        $skill = $event->skill;
        $level = $event->level;

         //send email and notification
         $user->notify(new NewUserSkillNotification($user,$skill,$level));
    }
}
