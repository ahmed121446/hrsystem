<?php

namespace App\Listeners;

use App\Events\NewContract;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Notifications\NewContract as NewContractNotification;

class ContractNotification implements ShouldQueue
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewContract  $event
     * @return void
     */
    public function handle(NewContract $event)
    {
        $user = $event->user;
        $contract = $event->contract;

        //send email and notification
        $user->notify(new NewContractNotification($user,$contract));
    }
}
