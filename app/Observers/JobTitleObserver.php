<?php

namespace App\Observers;

use App\JobTitle;

class JobTitleObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\JobTitle  $jobTitle
     * @return void
     */
    public function deleting(JobTitle $jobTitle)
    {
        foreach ($jobTitle->users()->get() as $user) {
            $user->delete();
        }
    }
}