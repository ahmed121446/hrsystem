<?php

namespace App\Observers;

use App\Salary;

class SalaryObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Salary  $salary
     * @return void
     */
    public function deleting(Salary $salary)
    {
        //
    }
}