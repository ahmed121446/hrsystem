<?php

namespace App\Observers;

use App\Contract;

class ContractObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Contract  $contract
     * @return void
     */
    public function deleting(Contract $contract)
    {
        foreach ($contract->salaries()->get() as $salary) {
            $salary->delete();
        }
    }
}