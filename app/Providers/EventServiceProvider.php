<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewEmployee' => [
            'App\Listeners\SupervisorNotification',
        ],
        'App\Events\NewSalary' => [
            'App\Listeners\SalaryNotification',
        ],

        'App\Events\NewContract' => [
            'App\Listeners\ContractNotification',
        ],

        'App\Events\AddedSkill' => [
            'App\Listeners\SkillNotification',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
