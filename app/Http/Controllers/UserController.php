<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JobTitle;
use App\City;
use App\Country;
use App\UserType;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Backend\UserRepository;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['type','jobTitle','jobTitle.department','supervisor'])
                    ->filterName( request('name') )
                    ->filterEmail( request('email') )
                    ->filterNationalId( request('national_id') )
                    ->filterDepartment( request('dept_name') )
                    ->filterJopTitle( request('jopTitle') )
                    ->filterSupervisorName( request('supervisor') )
                    ->paginate(10);
        return view('user.all',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            $this->flash('User can not be found' , 'danger');
            return redirect()->back();
        }
        $jobTitles = JobTitle::all();
        $employees = User::all();
        $countries = Country::all();
        $types = UserType::all();

        return view('user.edit',compact('user','jobTitles','employees','countries','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id ,UserRepository $repository)
    {
        $user =  User::find($id);
        $user_image = $repository->getUserImage($user);
        if ($request->has('image')) {
            if ($user_image) {
                Storage::delete($user->image); //delete user image
            }
            $image = $repository->storeUserImage($request);//store new image
            $update_image = $user->update([
                "image" =>  $image,
            ]);
            if (!$update_image) {
                $this->flash('User can not be updated' , 'danger');
                return redirect()->back();
            }
        }
        $updated = $user->update([
            'job_title_id'              => $request->get('job_title_id'), 
            'name_en'                   => $request->get('name_en'), 
            'name_ar'                   => $request->get('name_ar'), 
            'national_id'               => $request->get('national_id'), 
            'email'                     => $request->get('email'), 
            'address'                   => $request->get('address'), 
            'dob'                       => $request->get('dob'), 
            'total_vacation_days'       => $request->get('total_vacation_days'), 
            'years_of_experience'       => $request->get('years_of_experience'), 
            'supervisor_id'             => $request->get('supervisor_id'), 
            'mobile_number'             => $request->get('mobile_number'), 
            'rank'                      => $request->get('rank'), 
            'gender'                    => $request->get('gender'), 
            'country_id'                   => $request->get('country'), 
            'type_id'                   => $request->get('employee_type'), 
            'start_hire_date'           => $request->get('start_hire_date'), 
            'social_insurance_Number'   => $request->get('social_insurance_Number'), 
        ]);

        if (!$updated) {
            $this->flash('User can not be updated' , 'danger');
        }else{
            $this->flash('User updated successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id);
        if (!$User) {
            $this->flash('User can not be found' , 'danger');
        }
        if ($User->delete()) {
            $this->flash('User deleted successfully' , 'success');
        }else{
            $this->flash('User can not be deleted' , 'danger');
        }
        return redirect()->back();
    }

}
