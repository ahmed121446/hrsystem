<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Salary;
use App\Recruitment;
use App\SocialStatus;
use App\FamilyMember;
use App\JobTitle;
use App\UserType;
use App\City;
use App\Country;
use App\Skill;
use App\SkillLevel;
use App\UserSkill;
use Carbon\Carbon;

use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\UserPasswordRequest;
use App\Http\Requests\UserSkillRequest;
use App\Repositories\Backend\EmployeeRepository;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::with(['type','jobTitle','jobTitle.department','supervisor'])
                        ->Employees()
                        ->filterName( request('name') )
                        ->filterEmail( request('email') )
                        ->filterNationalId( request('national_id') )
                        ->filterDepartment( request('dept_name') )
                        ->filterJopTitle( request('jopTitle') )
                        ->filterSupervisorName( request('supervisor') )
                        ->paginate(10);
        return view('user.employees.all',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobTitles = JobTitle::all();
        $employees = User::all();
        $countries = Country::all();
        $types = UserType::all();
        return view('user.employees.create', compact('jobTitles','employees','countries','types'));
    }

    /**
     * Show the form for change password.
     *
     * @return \Illuminate\Http\Response
     */
    public function passwordChangeView($id){
        $user = User::find($id);
        if (!$user) {
            $this->flash('User can not be found' , 'danger');
            return redirect()->back();
        }
        if ($user->id == auth()->user()->id) {
            return view('user.password.edit',compact('user'));
        }else{
            $this->flash('you are not authorized' , 'danger');
            return redirect()->back();
        }
    }

    public function passwordChange(UserPasswordRequest $request , $id){        
        $user = User::find($id);
        $pass = \Auth::user()->password;
        $pass_entered = $request->get('old_password');
        if (!Hash::check( $pass_entered, $pass )) {
            $this->flash('old password is not correct' , 'danger');
            return redirect()->back();
        }else{
            $update = $user->update(['password' => bcrypt($pass_entered)]);
            if (!$update) {
                $this->flash('can not update password' , 'danger');
                return redirect()->back();
            }else{
                $this->flash('password updated successfully' , 'success');
                return redirect()->route('employee',$user->id);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request , EmployeeRepository $repository)
    {
        $image = $repository->storeUserImage($request);
        $is_valid_age = $repository->ageValidation($request);
        if(!$is_valid_age){
            $this->flash('Age' , 'danger','Diff between hiring date and date of birth must more that 15 years');
            return redirect()->back();
        }

        $supervisor = User::where('id',$request->get('supervisor_id'))->first();
        if(!$supervisor){
            $this->flash('supervisor can not be added' , 'danger');
            return redirect()->back();
        }
        $user = User::create([
            'job_title_id'              => $request->get('job_title_id'), 
            'name_en'                   => $request->get('name_en'), 
            'name_ar'                   => $request->get('name_ar'), 
            'national_id'               => $request->get('national_id'), 
            'email'                     => $request->get('email'), 
            'address'                   => $request->get('address'), 
            'password'                  => bcrypt('111111111'), 
            'default_password'          => bcrypt('111111111'), 
            'dob'                       => $request->get('dob'), 
            'total_vacation_days'       => $request->get('total_vacation_days'), 
            'years_of_experience'       => $request->get('years_of_experience'), 
            'supervisor_id'             => $request->get('supervisor_id'), 
            'mobile_number'             => $request->get('mobile_number'), 
            'rank'                      => $request->get('rank'), 
            'gender'                    => $request->get('gender'), 
            'country_id'                   => $request->get('country'), 
            'type_id'                   => $request->get('employee_type'), 
            'start_hire_date'           => $request->get('start_hire_date'), 
            'social_insurance_Number'   => $request->get('social_insurance_Number'), 
            "image" =>  $image,
        ]);
        
        $socialStatus =  SocialStatus::create([
            'user_id' => $user->id,
            'is_bread_winner' => ( $request->has('is_bread_winner') ) ? true : false ,
        ]);

        if(!$user){
            $this->flash('employee can not be added' , 'danger');
        }else{
            $this->flash('employee added successfully' , 'success');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $employee = User::with(['type','city','city.country',
        //                         'userSkills','jobTitle',
        //                         'jobTitle.department','socialStatus'
        //                         ])->where('id',$id)
        //                         //->employees()
        //                         ->first();
        $employee = User::with(['type','country',
                                'userSkills','jobTitle',
                                'jobTitle.department','socialStatus'
                                ])->where('id',$id)
                                //->employees()
                                ->first();
        if (!$employee) {
            $this->flash('employee can not be found , it could be admin or super admin' , 'danger');
            return redirect()->back();
        }

        $contracts = $employee->contracts()->orderBy('is_active','desc')->paginate(8, ['*'], 'Contract_page');
        $experiences = $employee->experiences()->orderBy('start_date','desc')->paginate(8, ['*'], 'Experiance_page');
        $certificates = $employee->certificates()->orderBy('date','desc')->paginate(8, ['*'], 'Certificate_page');
        $recruitment = Recruitment::where('user_id',$id)->first();
        $familyMembers = $employee->socialStatus->familyMembers()->paginate(8, ['*'], 'Family_page');
        
        $salaries_ids = [];
        $all_contracts = $employee->contracts;
        foreach ($all_contracts as $contract ) {
            foreach ($contract->salaries as $salary ) {
                array_push($salaries_ids,$salary->id);
            }
        }

        $salaries = Salary::with('contract')->whereIn('id',$salaries_ids)->latest()->get();
        return view('user.employees.show',compact('employee','contracts','experiences','certificates','recruitment','familyMembers','salaries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function createSkill($id)
    {
        $employee = User::find($id);
        if (!$employee) {
            $this->flash('employee can not be found' , 'danger');
            return redirect()->back();
        }
        $skills = Skill::all();
        $skillLevels = SkillLevel::all();
        return view('user.employees.create_user_skill',compact('employee','skills','skillLevels'));
    }

    public function storeSkill(UserSkillRequest $request)
    {
        $user_skill = UserSkill::create([
            'user_id' => $request->get('user_id'),
            'skill_id' => $request->get('skill'),
            'skill_level_id' => $request->get('skill_level'),
            'date_acquired' => $request->get('date_acquired'),
        ]);
        if(!$user_skill){
            $this->flash('user skill can not be added' , 'danger');
        }else{
            $this->flash('user skill added successfully' , 'success');
        }
        return redirect()->back();
    }


    public function team($id){
        $employee = User::find($id);
        if (!$employee) {
            $this->flash('supervisor can not be found' , 'danger');
            return redirect()->back();
        }
        if (!$employee->isSupervisor()) {
            $this->flash('not supervisor' , 'warning');
            return redirect()->back();
        }
        $team = User::supervisorTeam($employee->id)->paginate(10);
        return view('user.supervisor.team',compact('employee','team'));
    }

}
