<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function flash( $message, $type , $sub_message = null){
        session()->flash('message',$message);
        session()->flash('type',$type);
        if ($sub_message) {
            session()->flash('sub-message',$sub_message);
        }
    }
}
