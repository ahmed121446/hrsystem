<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SocialStatus;

class SocialStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $socialStatus = SocialStatus::find($id);
        if (!$socialStatus) {
            $this->flash('social status can not be found' , 'danger');
            return redirect()->back();
        }
        if ($request->has('is_bread_winner') && $socialStatus->is_bread_winner) {
            $this->flash('social status is already active' , 'warning');
        }else if (!$request->has('is_bread_winner') && !$socialStatus->is_bread_winner){
            $this->flash('social status is already not active' , 'warning');
        }else if ($request->has('is_bread_winner') && !$socialStatus->is_bread_winner) {
            $updated = $socialStatus->update([
                'is_bread_winner'=>true,
            ]);
            if(!$updated){
                $this->flash('social status can not be updated' , 'danger');
            }else{
                $this->flash('social status updated successfully' , 'success');
            }
        }else if(!$request->has('is_bread_winner') && $socialStatus->is_bread_winner){
            $updated = $socialStatus->update([
                'is_bread_winner'=>false,
            ]);
            if(!$updated){
                $this->flash('social status can not be updated' , 'danger');
            }else{
                $this->flash('social status updated successfully' , 'success');
            }
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
