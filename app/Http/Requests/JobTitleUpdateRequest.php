<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\JobTitle;
class JobTitleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $jobTitle = JobTitle::find($this->id);
        return [
            'name_en'=>'required | min:2 | max:20 | unique:job_titles,name_en,'.$jobTitle->id,
            'name_ar'=>'required | min:2 | max:20 | unique:job_titles,name_ar,'.$jobTitle->id,
            'department_id'=>'required | numeric | exists:departments,id',
        ];
    }
}
