<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request);
        return [
            'user_id' => 'required ',
            'start_date' => 'required | date ',
            'end_date' => 'required | date',
            'where' => 'required  ',
            'role_en' => 'required ',
            'role_ar' => 'required ',
            'description_en' => 'required | min:20 ',
            'description_ar' => 'required | min:20 '
        ];
    }
}
