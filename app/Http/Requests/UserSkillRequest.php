<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'skill' => 'required | exists:skills,id',
            'skill_level' => 'required | exists:skill_levels,id',
            'user_id'=> 'required | exists:users,id',
            'date_acquired'=> 'required',
        ];
    }
}
