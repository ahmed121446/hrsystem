<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Department;

class DepartmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $department = Department::find($this->id);
        return [
            'name_en'=>'required | min:2 | max:20 | unique:departments,name_en,'.$department->id,
            'name_ar'=>'required | min:2 | max:20 | unique:departments,name_ar,'.$department->id,
            'description_en'=>'required | min:15 ',
            'description_ar'=>'required | min:15 ',
        ];
    }
}
