<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en'                   => ' required | max:30 | min:2 ' ,
            "name_ar"                   => ' required | max:30 | min:2 ' ,
            "email"                     => ' required | unique:users,email'  ,
            "address"                   => ' required ' ,
            "total_vacation_days"       => ' required | numeric' ,
            "country"                   => ' required | numeric' ,
            "national_id"               => ' required | numeric | unique:users,national_id' ,
            "employee_type"             => ' required | numeric' ,
            "gender"                    => ' required ' ,
            "mobile_number"             => ' required | unique:users,mobile_number' ,
            "job_title_id"              => ' required | numeric' ,
            "social_insurance_Number"   => ' required | numeric | unique:users,social_insurance_Number' ,
            "image"                     => ' nullable ' ,
            "dob"                       => ' required ' ,
            "years_of_experience"       => ' required | numeric' ,
            "supervisor_id"             => ' required | numeric' ,
            "start_hire_date"           => ' required ' ,
            "rank"                      => ' required ' ,
        ];
    }
}
