<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Services\MultiLang;


class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( \Session::has('locale')) {
            \App::setLocale(\Session::get('locale'));
            Carbon::setLocale(\Session::get('locale'));

            $api_lang_suffix = '_' . \Session::get('locale');
            \App::singleton(MultiLang::class, function() use($api_lang_suffix){
                return new MultiLang($api_lang_suffix);
            });
        }else{
            $api_lang_suffix = '_' . \App::getLocale();
            \App::singleton(MultiLang::class, function() use($api_lang_suffix){
                return new MultiLang($api_lang_suffix);
            });
        }
        return $next($request);
 
    }
}
