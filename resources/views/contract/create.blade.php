@extends('master')


@section('title')
    Create Contract
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('contracts/create.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       

        <div class="br-pagebody">
          <div class="br-section-wrapper">
                  <h6 class="tx-gray-800 tx-uppercase tx-bold tx-14 mg-t-80 mg-b-10">{{ __('contracts/create.CONTRACT_TABLE')}}</h6>
                  <p class="mg-b-30 tx-gray-600">{{ __('contracts/create.SUB_TITLE_CONTRACT_TABLE')}}</p>
                  <div class="form-layout form-layout-2">
                    <form action="{{route('store.contract')}}" method="post">
                        {{csrf_field()}}
                        @include('contract.create_form')
                    </form>
                  </div>
          </div><!-- br-section-wrapper -->
        </div>





@endsection

@section('script')
@endsection







