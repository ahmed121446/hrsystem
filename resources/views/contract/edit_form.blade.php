<div class="row no-gutters">




        {{-- Name AR --}}
        <div class="col-md-12 ">
            <div class="form-group ">
                <label class="form-control-label {{ ($errors->has('user_id')) ? ' is-invalid' : '' }} ">
                    {{ __('contracts/edit.FORM_USER')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input class="form-control" type="text" value="{{$contract->user->name}}" disabled>
                    <input  type="hidden" value="{{$contract->user->id}}" name="user_id">
                <span class="form-error">
                            <small>{{ $errors->first('user_id') }}</small>
                    </span>
            </div>
        </div>
    
    
        {{-- Monthly Salary --}}
        <div class="col-md-4 ">
            <div class="form-group bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('monthly_salary')) ? ' is-invalid' : '' }} ">
                    {{ __('contracts/edit.FORM_MONTHLY_SALARY')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="number" value="{{$contract->monthly_salary}}" name="monthly_salary" required placeholder="{{ __('contracts/edit.FORM_MONTHLY_SALARY_PLACEHOLDER')}}">
                <span class="form-error">
                        <small>{{ $errors->first('monthly_salary') }}</small>
                </span>
            </div>
        </div>
    
        {{-- Currancy --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('currancy')) ? ' is-invalid' : '' }}">
                    {{ __('contracts/edit.FORM_CURRANCY')}}:
                        <span class="tx-danger">*</span>
                    </label>
                <select name="currancy" class="form-control select2-hidden-accessible" data-placeholder="Choose currancy" tabindex="-1" aria-hidden="true">
                        <optgroup label="{{ __('contracts/edit.FORM_CURRANCY_PLACEHOLDER')}}">
                            <option value="$" {{( $contract->currancy == '$')? 'selected' : '' }}>$</option>
                            <option value="€" {{( $contract->currancy == '€')? 'selected' : '' }}>€</option>
                            <option value="﷼" {{( $contract->currancy == '﷼')? 'selected' : '' }}>﷼</option>
                        </optgroup>
                    </select>
                <span class="form-error">
                        <small>{{ $errors->first('currancy') }}</small>
                    </span>
            </div>
        </div>
    
    
        {{-- Active --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('is_active')) ? ' is-invalid' : '' }}">
                    {{ __('contracts/edit.FORM_ACTIVE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <label class="switch">
                    <input type="checkbox" name="is_active"  {{( $contract->is_active ) ? 'checked' : '' }} >
                    <span class="slider round"></span>
                </label>
                <span class="form-error">
                    <small>{{ $errors->first('is_active') }}</small>
                </span>
            </div>
        </div>
    
    
    
    
        {{-- Start Date --}}
        <div class="col-md-6">
            <div class="form-group bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('start_date')) ? ' is-invalid' : '' }}">
                    {{ __('contracts/edit.FORM_START_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="date" value="{{$contract->start_date}}" name="start_date" required>
                <span class="form-error">
                        <small>{{ $errors->first('start_date') }}</small>
                </span>
            </div>
        </div>
        {{-- End Date --}}
        <div class="col-md-6 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('end_date')) ? ' is-invalid' : '' }}">
                    {{ __('contracts/edit.FORM_END_DATE')}} :
                        <span class="tx-danger">*</span>
                    </label>
                <input class="form-control" type="date" name="end_date" value="{{$contract->end_date}}" required>
                <span class="form-error">
                    <small>{{ $errors->first('end_date') }}</small>
                </span>
            </div>
        </div>
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button class="btn btn-info">{{ __('contracts/edit.CREAT_BUTTON')}}</button>
        <a href="{{route('contracts')}}" class="btn btn-secondary">{{ __('contracts/edit.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-group -->