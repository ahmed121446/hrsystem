<div class="form-layout form-layout-1">
        <div class="row mg-b-25">
    
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                        Name EN:
                        <span class="tx-danger">*</span>
                    </label>
                    <input class="form-control" type="text" value="{{$department->name_en}}" name="name_en" placeholder="Enter name in english">
                    <span class="form-error">
                        <small>{{ $errors->first('name_en') }}</small>
                    </span>
                </div>
            </div>
            <!-- col-6 -->
            <div class="col-lg-6">
                <div class="form-group">
                        <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }}">
                                Name AR:
                                <span class="tx-danger">*</span>
                            </label>
                            <input class="form-control" type="text" value="{{$department->name_ar}}"  name="name_ar" placeholder="أدخل الاسم باللغة العربية">
                            <span class="form-error">
                                <small>{{ $errors->first('name_ar') }}</small>
                            </span>
                </div>
            </div>
            <!-- col-6 -->
    
            <div class="col-lg-6">
                <div class="form-group">
                        <label class="form-control-label {{ ($errors->has('description_en')) ? ' is-invalid' : '' }}">
                                Description EN:
                                <span class="tx-danger">*</span>
                            </label>
                            <textarea class="form-control"  name="description_en" placeholder="Lorem ipsum dolor sit amet...." cols="30" rows="10">{{$department->description_en}}</textarea>
                            <span class="form-error">
                                <small>{{ $errors->first('description_en') }}</small>
                            </span>
                </div>
            </div>
            <!-- col-6 -->
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label {{ ($errors->has('description_ar')) ? ' is-invalid' : '' }}">
                        Description AR:
                        <span class="tx-danger">*</span>
                    </label>
                    <textarea class="form-control"  name="description_ar" placeholder="Lorem ipsum dolor sit amet...." cols="30" rows="10">{{$department->description_ar}}</textarea>
                    <span class="form-error">
                        <small>{{ $errors->first('description_ar') }}</small>
                    </span>
                </div>
            </div>
            <!-- col-6 -->
    
    
        </div>
        <!-- row -->
    
        <div class="form-layout-footer">
            <button type="submit" class="btn btn-info">Submit Form</button>
            <button class="btn btn-secondary">Cancel</button>
        </div>
        <!-- form-layout-footer -->
    </div>