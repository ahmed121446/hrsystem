@extends('master')


@section('title')
    All Departments
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('departments/dept.header')}} </h4>
          <p class="mg-b-0"></p>
        </div>

       
        


        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="br-pagebody pd-x-20 pd-sm-x-30">
                    <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('departments/dept.DEPT_TABLE')}} </h6>
                    <p class="  mg-lg-b-50"> {{ __('departments/dept.SUB_TITLE_DEPT_TABLE')}} </p>
                    <a href="{{route('create.department')}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6 ' }} tx-12">
                        <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
                        {{ __('departments/dept.CREAT_BUTTON')}}
                    </a>
                    

                    <div class="row">
                            <div class="col-md-9">
                                <form action="{{route('departments')}}" method="get">
                                    <div class="input-group">
                                        <input type="text" class=" form-control" style="border-radius: 0px;" value="{{ request('name')}}" name="name" placeholder="{{ __('departments/dept.SEARCH_FOR_NAME')}}">
                                        <span class="input-group-btn">
                                            <button  class="btn bd bg-white tx-gray-600" style="border-radius: 0px;" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <!-- input-group -->
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('departments')}}" class="btn btn-primary">{{ __('departments/dept.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>



                    <div class=" rounded table-responsive">
                        <table class="table table-striped table-bordered table-hover mg-b-0">
                            <thead class="thead-colored thead-dark">
                                <tr>
                                        <td>{{__('departments/dept.TABLE_HEAD_NAME')}}</td>
                                        <td>{{__('departments/dept.TABLE_HEAD_DEPT')}}</td>
                                        <td>{{__('departments/dept.TABLE_HEAD_OPTIONS')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($departments as $department)
                                    <tr>
                                        <td class="user-info-table" style="width: {{(app()->getLocale() == 'ar') ? '150px;' : '120px;' }}">
                                            {{$department->name}}
                                            <div>
                                                <small><span class="label label-danger">{{__('departments/dept.TABLE_JOBTITLE')}} : {{$department->jobTitles->count()}} </span></small> 
                                            </div>
                                        </td>
                                        <td class="user-info-table">
                                                {{$department->description }}
                                        </td>
                                        <td class="options" style="width: {{(app()->getLocale() == 'ar') ? '80px;' : '60px;' }}">
                                            <a href="{{route('department.edit',$department->id)}}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="" class="btnDelete" data-department_id="{{$department->id}}">
                                                <i class="fa fa-trash"></i>               
                                            </a>                
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $departments->appends($_GET)->links() }}
                </div> 
            </div>
        </div>

        @include('include.modals.delete')
@endsection
@section('script')
          <script src="{{asset('js/delete-config.js')}}"></script>
          <script src="{{asset('js/department/delete.js')}}"></script>
@endsection






