<div class="row no-gutters">
        {{-- Name EN --}}
        <div class="col-md-4">
            <div class="form-group ">
                <label class="form-control-label">
                    Employee Name:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$employee->name}}" type="text" name="name_en">
            </div>
        </div>
    
    
        {{-- Contract ID --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label">
                    Contract ID:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control" type="number" value="{{$active_contract->id}}">
                <input type="hidden" name="contract_id" value="{{$active_contract->id}}">
            </div>
        </div>
    
    
    
        {{-- Date --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label">
                    Date:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control" type="month" value="{{$today}}" name="date" >
                <input type="hidden" name="date" value="{{$today}}">
            </div>
        </div>
    
    
        {{-- Contract Monthly Salary --}}
        <div class="col-md-4 ">
            <div class="form-group bd-t-0-force">
                <label class="form-control-label">
                    Contract Monthly Salary:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled value="{{ $active_contract->monthly_salary }}" class="form-control" type="number"   >
                <input type="hidden" name="monthly_salary" value="{{$active_contract->monthly_salary}}">
            </div>
        </div>


        {{-- Incentive --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('incentive')) ? ' is-invalid' : '' }}">
                        Incentive:
                    </label>
                    <input value="{{ old('incentive') }}" class="form-control" type="number" name="incentive" placeholder="310.50" step="0.01">
                    <span class="form-error">
                        <small>{{ $errors->first('incentive') }}</small>
                    </span>
                </div>
        </div>

        {{--Deduction --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('deduction')) ? ' is-invalid' : '' }}">
                        Deduction:
                    </label>
                    <input value="{{ old('deduction') }}" class="form-control" type="number" name="deduction" placeholder="310.50" step="0.01">
                    <span class="form-error">
                        <small>{{ $errors->first('deduction') }}</small>
                    </span>
                </div>
        </div>
    
       
    
    















         {{-- Other --}}
         <div class="col-md-3 ">
                <div class="form-group bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('other')) ? ' is-invalid' : '' }}">
                        Other:
                    </label>
                    <input value="{{ old('other') }}" class="form-control" type="number" name="other" placeholder="200.50" step="0.01">
                    <span class="form-error">
                            <small>{{ $errors->first('other') }}</small>
                    </span>
                </div>
            </div>
    
    
            {{-- allowance --}}
            <div class="col-md-3 mg-t--1 mg-md-t-0">
                    <div class="form-group mg-md-l--1 bd-t-0-force">
                        <label class="form-control-label {{ ($errors->has('allowance')) ? ' is-invalid' : '' }}">
                            Allowance:
                        </label>
                        <input value="{{ old('allowance') }}" class="form-control" type="number" name="allowance" placeholder="200.50" step="0.01">
                        <span class="form-error">
                            <small>{{ $errors->first('allowance') }}</small>
                        </span>
                    </div>
            </div>
    
            {{-- tax --}}
            <div class="col-md-3 mg-t--1 mg-md-t-0">
                    <div class="form-group mg-md-l--1 bd-t-0-force">
                        <label class="form-control-label {{ ($errors->has('tax')) ? ' is-invalid' : '' }}">
                            Tax:
                        </label>
                        <input value="{{ old('tax') }}" class="form-control" type="number" name="tax" placeholder="200" step="0.01">
                        <span class="form-error">
                            <small>{{ $errors->first('tax') }}</small>
                        </span>
                    </div>
            </div>



     {{-- loan --}}
     <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1 bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('loan')) ? ' is-invalid' : '' }}">
                    Loan:
                </label>
                <input value="{{ old('loan') }}" class="form-control" type="number" name="loan" placeholder="200.50" step="0.01">
                <span class="form-error">
                        <small>{{ $errors->first('loan') }}</small>
                </span>
            </div>
        </div>

    
    
    
    
    
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button class="btn btn-info">Submit Form</button>
        <button class="btn btn-secondary">Cancel</button>
    </div>
    <!-- form-group -->