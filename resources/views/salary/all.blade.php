@extends('master')


@section('title')
    All Salaries
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('salaries/salary.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

        <div class="br-pagebody">
                <div class="tab-content br-profile-body">
                  <div class="tab-pane fade active show" id="posts" aria-expanded="true">
                    <div class="row">
                      @include('salary.include.left')    <!-- left part -->
                      @include('salary.include.right')   <!-- right part -->
                    </div><!-- row -->
                  </div><!-- tab-pane -->       
                </div>
        </div>

        @include('include.modals.delete')

@endsection

@section('script')
          <script src="{{asset('js/delete-config.js')}}"></script>
          <script src="{{asset('js/salary/delete.js')}}"></script>
@endsection






