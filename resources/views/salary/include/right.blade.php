<div class="col-lg-9">
        <div class="media-list bg-white rounded shadow-base tab-container">
                <div class=" pd-t-30 pd-r-20 pd-b-20 pd-l-20">
                        <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('salaries/salary.SALARY_TABLE')}}</h6>
                        <p class="  mg-lg-b-10">
                            <small>{{$date}}</small>
                        </p>
                        
                        <div class="row">
                            <div class="col-md-9">
                                <form action="{{route('salaries')}}" method="get">
                                    <div class="input-group">
                                        <input type="text" class=" form-control" style="border-radius: 0px;" value="{{ request('emp_name')}}" name="emp_name" placeholder="{{ __('salaries/salary.SEARCH_FOR_NAME')}}...">
                                        <span class="input-group-btn" >
                                            <button  class="btn bd bg-white tx-gray-600" style="border-radius: 0px;" type="submit"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <!-- input-group -->
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('salaries')}}" class="btn btn-primary">{{ __('salaries/salary.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>
        
                        <div class=" rounded table-responsive">
                            <table class="table table-striped table-bordered table-hover mg-b-0">
                                <thead class="thead-colored thead-dark">
                                    <tr>
                                        <td>#</td>
                                        <td> {{ __('salaries/salary.TABLE_HEAD_Employee')}}</td>
                                        <td> {{ __('salaries/salary.TABLE_HEAD_Date')}}</td>
                                        <td> {{ __('salaries/salary.TABLE_HEAD_Contract')}}</td>
                                        <td> {{ __('salaries/salary.TABLE_HEAD_Total')}}</td>
                                        <td> {{ __('salaries/salary.TABLE_HEAD_OPTIONS')}}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($salaries as $salary)
                                        <tr>
                                            <td>
                                                {{$salary->id }}
                                            </td>
                                            <td class="user-info-table">
                                                <a href="{{route('employee',$salary->contract->user->id.'?Salarie_page')}}">
                                                    @if ($salary->contract->user->image == null)
                                                        <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                    @else
                                                        <img src="{{ asset('storage/images/users/'. $salary->contract->user->image) }}" class="user-image rounded-circle" alt="">
                                                    @endif
                                                    {{$salary->contract->user->name}}
                                                </a>  
                                                <div class="user-info" >   
                                                    <small><span class="label ">{{ __('salaries/salary.TABLE_JOBTITLE')}}</span>  : {{$salary->contract->user->jobTitle->name}}</small> 
                                                </div>
                                                <div class="user-info" >
                                                    @if ($salary->contract->user->type->name_en == 'employee')
                                                    <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                    @elseif($salary->contract->user->type->name_en == 'admin')
                                                    <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                    @elseif($salary->contract->user->type->name_en == 'superAdmin')
                                                    <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                    @endif
                                                    <small>{{$salary->contract->user->type->name}} </small>
                                                </div> 
                                            </td>
        
                                            <td>
                                                    {{$salary->date }}
                                            </td>
                                            <td>
                                                <div class="display-flex">
                                                    <p>{{ __('salaries/salary.TABLE_ISACTIVE')}} : </p>
                                                    @if ($salary->contract->is_active)
                                                    <div class="circle circle-success"></div>
                                                    @else
                                                    <div class="circle circle-error"></div>
                                                    @endif
                                                </div>      
                                            </td>
        
                                           
                                            <td>
                                                <span class="label ">
                                                    {{number_format($salary->total) }} {{$salary->contract->currancy }}
                                                </span>
                                            </td>
                                            <td class="options">
                                                <a href="{{route('employee',$salary->contract->user->id.'?Salarie_page')}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="{{route('salary.edit',$salary->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="" class="btnDelete" data-salary_id="{{$salary->id}}">
                                                        <i class="fa fa-trash"></i>               
                                                </a>                     
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        
                        {{ $salaries->appends($_GET)->links() }}
                </div> 
        </div><!-- card -->
    </div><!-- col-lg-8 -->   <!-- right part -->








            
