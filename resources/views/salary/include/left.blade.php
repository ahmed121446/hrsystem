<div class="col-lg-3 mg-t-30 mg-lg-t-0">
    <div class="card pd-20 pd-xs-30 shadow-base bd-0">
        <h6 class="tx-gray-800 tx-uppercase tx-semibold tx-13 mg-b-25">{{ __('salaries/salary.TABLE_FILTER')}}</h6>

        <div class="display-flex">
                <i class="menu-item-icon fa fa-calendar tx-18"></i>
                <p class="tx-inverse mg-b-25 mg-l-15">
                    <a href="{{route('salaries')}}">
                        All
                    </a> 
                </p> 
        </div>
        @foreach ($archives as $archive)
            <div class="display-flex">
                <i class="menu-item-icon fa fa-calendar tx-18"></i>
                <p class="tx-inverse mg-b-25 mg-l-15">
                    <a href="?month={{$archive['month']}}&year={{$archive['year']}}" >
                        {{$archive['month']}} {{$archive['year']}}
                    </a>
                    
                </p> 
            </div>
        @endforeach
       
    </div><!-- card -->
</div><!-- col-lg-4 -->    <!-- left part -->