<!DOCTYPE html>
<html   lang="{{ app()->getLocale() }}" 
        dir="{{ (app()->getLocale() === 'ar') ? 'rtl' : '' }}" 
        class="{{ (app()->getLocale() === 'ar') ? 'rtl' : '' }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Login page">
    <meta name="author" content="Ahmed Magdy">

    <title>Login</title>

    <!-- vendor css -->
    <link rel="stylesheet" href="{{asset('css/temp-css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/temp-css/ionicons.css')}}">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{asset('css/temp-css/bracket.css')}}">
    <style>
        .lang{
            position: absolute;
            top: 10%;
            left: 25%;
        }
      

    </style>

  </head>

  <body>
        @if (app()->getLocale() == 'ar' )
            <a  href="/locale/en" class="btn btn-primary btn-icon lang pd-l-20 pd-r-20 tx-center">
                <div><i class="fa fa-globe mg-l-10"></i>EN</div>
            </a>     
        @elseif(app()->getLocale() == 'en' )
            <a  href="/locale/ar" class="btn btn-primary btn-icon lang pd-r-20  pd-l-20 tx-center">
                <div><i class="fa fa-globe mg-r-10"></i>AR</div>
            </a>    
        @endif
       
    <div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
                <div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal">[</span> {{__('login.head')}} <span class="tx-normal">]</span></div>
                <div class="tx-center mg-b-60">{{__('login.title')}}</div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="{{__('login.email_placeholder')}}" required>
                    @if ($errors->has('email'))
                        <ul class="parsley-errors-list filled">
                            <li class="parsley-required">{{ $errors->first('email') }}</li>
                        </ul>
                    @endif
                </div><!-- form-group -->
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="{{__('login.password_placeholder')}}" required>
                    @if ($errors->has('password'))
                    <ul class="parsley-errors-list filled">
                        <li class="parsley-required">{{ $errors->first('password') }}</li>
                    </ul>
                    @endif
                    <a class="tx-info tx-12 d-block mg-t-10" href="{{ route('password.request') }}">{{__('login.forget_password')}}</a>
                </div><!-- form-group -->
                <button type="submit" class="btn btn-info btn-block">{{ __('login.btn_login') }}</button>

            </div><!-- login-form -->
        </form>
    </div><!-- d-flex -->

  </body>
</html>
