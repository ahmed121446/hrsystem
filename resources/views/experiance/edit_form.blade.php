<div class="row no-gutters">
        {{-- Name EN --}}
        <div class="col-md-3">
            <div class="form-group ">
                <label class="form-control-label">
                    {{__('experience/edit.FORM_EMPLOYEE_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$experience->user->name}}" type="text" name="name_en">
                <input   value="{{$experience->user->id}}" type="hidden" name="user_id">
            </div>
        </div>
    
    
        {{-- start_date  --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('start_date')) ? ' is-invalid' : '' }}">
                    {{__('experience/edit.FORM_STARTDATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required class="form-control" type="date" name="start_date" value="{{$experience->start_date}}">
                <span class="form-error">
                        <small>{{ $errors->first('start_date') }}</small>
                </span>
            </div>
        </div>
    
    
        {{-- end_date --}}
        <div class="col-md-3 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('end_date')) ? ' is-invalid' : '' }}">
                    {{__('experience/edit.FORM_END_DATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required value="{{ $experience->end_date }}" class="form-control" type="date" name="end_date">
                <span class="form-error">
                        <small>{{ $errors->first('end_date') }}</small>
                </span>
            </div>
        </div>

         {{-- Where --}}
         <div class="col-md-3 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                    <label class="form-control-label {{ ($errors->has('where')) ? ' is-invalid' : '' }}">
                        {{__('experience/edit.FORM_WHERE')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ $experience->where }}" class="form-control" type="text" name="where" placeholder="Where">
                    <span class="form-error">
                            <small>{{ $errors->first('where') }}</small>
                    </span>
                </div>
            </div>






        {{--  Role In Arabic --}}
        <div class="col-md-6 ">
                <div class="form-group  bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('role_ar')) ? ' is-invalid' : '' }}">
                        {{__('experience/edit.FORM_ROLE_ARABIC')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ $experience->role_ar }}" class="form-control" type="text" name="role_ar" placeholder="Role In Arabic">
                    <span class="form-error">
                        <small>{{ $errors->first('role_ar') }}</small>
                    </span>
                </div>
        </div>

         {{--  Role In English --}}
         <div class="col-md-6 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('role_en')) ? ' is-invalid' : '' }}">
                        {{__('experience/edit.FORM_ROLE_ENGLISH')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input value="{{ $experience->role_en }}" class="form-control" type="text" name="role_en" placeholder=" Role In English">
                    <span class="form-error">
                        <small>{{ $errors->first('role_en') }}</small>
                    </span>
                </div>
        </div>


         {{--  Description In Arabic --}}
         <div class="col-md-6 ">
                <div class="form-group  bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('description_ar')) ? ' is-invalid' : '' }}">
                        {{__('experience/edit.FORM_DESCRIPTION_IN_ARABIC')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <textarea required cols="30" rows="10" name="description_ar"class="form-control">
                            {{ $experience->description_ar }}
                    </textarea>
                    <span class="form-error">
                        <small>{{ $errors->first('description_ar') }}</small>
                    </span>
                </div>
        </div>

         {{--  Description In English --}}
         <div class="col-md-6 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('description_en')) ? ' is-invalid' : '' }}">
                        {{__('experience/edit.FORM_DESCRIPTION_IN_ENGLISH')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <textarea required cols="30" rows="10" name="description_en"  class="form-control">
                            {{$experience->description_en }}
                    </textarea>
                    <span class="form-error">
                        <small>{{ $errors->first('description_en') }}</small>
                    </span>
                </div>
        </div>
    
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button type="submit" class="btn btn-info">{{__('experience/edit.CREAT_BUTTON')}}</button>
        <a href="{{route('employee',$experience->user->id.'?Experiance_page')}}" class="btn btn-secondary">{{__('experience/edit.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-group -->