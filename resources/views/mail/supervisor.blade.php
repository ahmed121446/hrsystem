@component('mail::message')
Hello {{$supervisor->name_en}}

New employee is added in your team <strong> {{$employee->name_en}}</strong> 

@component('mail::button', ['url' => "{{route('employee',$supervisor->id)}}"])
Your Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
