@component('mail::message')
Hello {{$user->name_en}}

New salary has been added for you.

@component('mail::table')
| Content       | Value         |
| ------------- |:-------------:|
| Date              | {{$salary->date}}         |
| Incentive         | {{($salary->incentive) ? $salary->incentive : '0.00'}}    |
| Deduction         | {{($salary->deduction) ? $salary->deduction : '0.00'}}    |
| Other             | {{($salary->other) ? $salary->other : '0.00'}}        |
| allowance         | {{($salary->allowance) ? $salary->allowance : '0.00'}}    |
| tax               | {{($salary->tax) ? $salary->tax : '0.00'}}          |
| loan              | {{($salary->loan) ? $salary->loan : '0.00'}}         |
| net               | {{$salary->net}}          |
| total             | {{$salary->total}}        |


@endcomponent

@component('mail::button', ['url' => "{{route('employee',$user->id)}}"])
Your Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
