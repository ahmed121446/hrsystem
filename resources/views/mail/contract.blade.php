@component('mail::message')
Hello {{$user->name_en}}

New contract has been added for you.

@component('mail::table')
| Content       | Value         |
| ------------- |:-------------:|
| Start Date        | {{$contract->start_date}}     |
| End Date          | {{$contract->end_date}}       |
| Active            | {{$contract->is_active}}      |
| Monthly Salary    | {{$contract->monthly_salary}} |
| Currancy          | {{$contract->currancy}}       |
| Total Annual      | {{$contract->total_annual}}   |


@endcomponent

@component('mail::button', ['url' => "{{route('employee',$user->id)}}"])
Your Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
