<div class="row no-gutters">
        {{-- Name EN --}}
        <div class="col-md-6">
            <div class="form-group ">
                <label class="form-control-label">
                    {{__('certificate/create.FORM_EMPLOYEE_NAME')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input disabled class="form-control is-invalid" value="{{$employee->name}}" type="text" name="name_en">
                <input   value="{{$employee->id}}" type="hidden" name="user_id">
            </div>
        </div>
    
    
        {{-- Date  --}}
        <div class="col-md-6 mg-t--1 mg-md-t-0">
            <div class="form-group mg-md-l--1">
                <label class="form-control-label {{ ($errors->has('date')) ? ' is-invalid' : '' }}">
                    {{__('certificate/create.FORM_STARTDATE')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required class="form-control" type="date" name="date" value="{{old('date')}}">
                <span class="form-error">
                        <small>{{ $errors->first('date') }}</small>
                </span>
            </div>
        </div>
    
    
        {{-- English Name --}}
        <div class="col-md-4 ">
            <div class="form-group bd-t-0-force">
                <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                    {{__('certificate/create.FORM_NAME_ENGLISH')}}:
                    <span class="tx-danger">*</span>
                </label>
                <input required value="{{ old('name_en') }}" class="form-control" type="text" name="name_en" placeholder="{{__('certificate/create.FORM_NAME_AR_PLACEHOLDER')}}">
                <span class="form-error">
                        <small>{{ $errors->first('name_en') }}</small>
                </span>
            </div>
        </div>


        {{--  Arabic Name --}}
        <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }}">
                        {{__('certificate/create.FORM_NAME_ARABIC')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <input required value="{{ old('name_ar') }}" class="form-control" type="text" name="name_ar" placeholder="{{__('certificate/create.FORM_NAME_EN_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('name_ar') }}</small>
                    </span>
                </div>
        </div>

         {{--  Arabic Name --}}
         <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                    <label class="form-control-label {{ ($errors->has('location')) ? ' is-invalid' : '' }}">
                        {{__('certificate/create.FORM_LOCATION')}}:
                    </label>
                    <input value="{{ old('location') }}" class="form-control" type="text" name="location" placeholder="{{__('certificate/create.FORM_LOCATION_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('location') }}</small>
                    </span>
                </div>
        </div>
    
    
    </div>
    <!-- row -->
    
    
    {{-- buttons --}}
    <div class="form-layout-footer bd pd-20 bd-t-0">
        <button class="btn btn-info">{{__('certificate/create.CREAT_BUTTON')}}</button>
        <a href="{{route('employee',$employee->id.'?Certificate_page')}}" class="btn btn-secondary">{{__('certificate/create.CREAT_CANCEL')}}</a>
    </div>
    <!-- form-group -->