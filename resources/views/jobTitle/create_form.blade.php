<div class="form-layout form-layout-1">
        <div class="row mg-b-25">
    
          <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label {{ ($errors->has('name_en')) ? ' is-invalid' : '' }}">
                    {{ __('jobTitles/create.FORM_NAME_EN')}}: 
                    <span class="tx-danger">*</span>
                </label>
                <input class="form-control" type="text" name="name_en" placeholder="{{ __('jobTitles/create.FORM_NAME_EN_PLACEHOLDER')}}">
                <span class="form-error">
                    <small>{{ $errors->first('name_en') }}</small>
                </span>
            </div>
          </div><!-- col-6 -->
          
    
          <div class="col-lg-4">
                <div class="form-group">
                    <label class="form-control-label {{ ($errors->has('name_ar')) ? ' is-invalid' : '' }}">
                        {{ __('jobTitles/create.FORM_NAME_AR')}}: 
                        <span class="tx-danger">*</span>
                    </label>
                    <input class="form-control" type="text" name="name_ar" placeholder="{{ __('jobTitles/create.FORM_NAME_AR_PLACEHOLDER')}}">
                    <span class="form-error">
                        <small>{{ $errors->first('name_ar') }}</small>
                    </span>
                </div>
              </div><!-- col-6 -->
    
              {{-- supervisor --}}
        <div class="col-md-4">
                <div class="form-group ">
                    <label class="form-control-label {{ ($errors->has('department_id')) ? ' is-invalid' : '' }}">
                        {{ __('jobTitles/create.FORM_DEPT')}}:
                        <span class="tx-danger">*</span>
                    </label>
                    <select name="department_id" class="form-control select2" data-placeholder="Choose Browser">
                        <option label="{{ __('jobTitles/create.FORM_DEPT_PLACEHOLDER')}}" selected></option>
                        @foreach ($departments as $department)
                            <option value="{{$department->id}}" {{ (old('department_id') == $department->id )?'selected' :'' }}>{{$department->name}}</option>
                        @endforeach
                    </select>
                    <span class="form-error">
                            <small>{{ $errors->first('department_id') }}</small>
                    </span>
                </div>
            </div>
              
    
    
        </div><!-- row -->
    
        <div class="form-layout-footer">
          <button type="submit" class="btn btn-info">{{ __('jobTitles/create.CREAT_BUTTON')}}</button>
          <a  href="{{route('jobTitles')}}" class="btn btn-secondary">{{ __('jobTitles/create.CREAT_CANCEL')}}</a>
        </div><!-- form-layout-footer -->
      </div>