@extends('master') 
@section('title') All job Titles
@endsection
 {{-- 
@section('sub-nav')
<div class="br-pageheader pd-y-15 pd-l-20">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
        <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
    </nav>
</div>
<!-- br-pageheader -->
@endsection
 --}} 
@section('content')

<div class="pd-x-30 pd-t-30">
    <h4 class="tx-gray-800 mg-b-5">{{ __('jobtitles/jobtitle.header')}}</h4>
    <p class="mg-b-0"></p>
</div>





<div class="br-pagebody">
    <div class="br-section-wrapper">
        <div class="br-pagebody pd-x-20 pd-sm-x-30">
            <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('jobtitles/jobtitle.JOBTITLE_TABLE')}}</h6>
            <p class="  mg-lg-b-50">{{ __('jobtitles/jobtitle.SUB_TITLE_JOBTITLE_TABLE')}}</p>
            <a href="{{route('create.jobTitle')}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6 ' }}  tx-12">
                <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }}"></i>
                {{ __('jobtitles/jobtitle.CREAT_BUTTON')}}
            </a>


            <div class="row">
                <div class="col-md-10">
                    <form action="{{route('jobTitles')}}" method="get" class="form-inline">
                        <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30 ' }}">
                            <input type="text" value="{{ request('name')}}" class="form-control" name="name" placeholder="{{ __('jobtitles/jobtitle.SEARCH_FOR_NAME')}}">
                        </div>
                        <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }}">
                            <input type="text" value="{{ request('dept_name')}}" class="form-control" name="dept_name" placeholder="{{ __('jobtitles/jobtitle.SEARCH_DEPT_NAME')}}">
                        </div>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary">{{ __('jobtitles/jobtitle.SEARCH_SEARCH_BTN')}}</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                    <a href="{{route('jobTitles')}}" class="btn btn-dark"> {{ __('jobtitles/jobtitle.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                </div>
            </div>
            <hr>



            <div class=" rounded table-responsive">
                <table class="table table-striped table-bordered table-hover mg-b-0">
                    <thead class="thead-colored thead-dark">
                        <tr>
                            <td>{{ __('jobtitles/jobtitle.TABLE_HEAD_NAME')}}</td>
                            <td>{{ __('jobtitles/jobtitle.TABLE_HEAD_DEPT')}}</td>
                            <td>{{ __('jobtitles/jobtitle.TABLE_HEAD_OPTIONS')}}</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jobTitles as $jobTitle)
                        <tr>
                            <td class="user-info-table">
                                {{$jobTitle->name}}
                                <div>
                                    <small><span class="label label-danger">{{ __('jobtitles/jobtitle.TABLE_EMPLOYEES')}}  : {{$jobTitle->users->count()}} </span></small>
                                </div>
                            </td>
                            <td class="user-info-table">
                                {{$jobTitle->department->name }}
                            </td>
                            <td class="options">
                                <a href="{{route('jobTitle.edit',$jobTitle->id)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="" class="btnDelete" data-job_id="{{$jobTitle->id}}">
                                     <i class="fa fa-trash"></i>               
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {{ $jobTitles->appends($_GET)->links() }}
        </div>
    </div>
</div>
    @include('include.modals.delete')
@endsection
 
@section('script')
<script src="{{asset('js/delete-config.js')}}"></script>
<script src="{{asset('js/jobTitle/delete.js')}}"></script>
@endsection