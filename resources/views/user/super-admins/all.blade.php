@extends('master')


@section('title')
    All Super-Admins 
@endsection

{{-- @section('sub-nav')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item active" href="{{route('users')}}">Employees</a>
        </nav>
      </div><!-- br-pageheader -->
@endsection --}}

@section('content')
        
        <div class="pd-x-30 pd-t-30">
          <h4 class="tx-gray-800 mg-b-5">{{ __('users/super_admin.header')}}</h4>
          <p class="mg-b-0"></p>
        </div>

       
        


        <div class="br-pagebody">
            <div class="br-section-wrapper">
                <div class="br-pagebody pd-x-20 pd-sm-x-30">
                    <h6 class="tx-inverse tx-uppercase tx-bold tx-14 ">{{ __('users/super_admin.USERS_TABLE')}}</h6>
                    <p class="mg-b-25 mg-lg-b-50">{{ __('users/super_admin.SUB_TITLE_USERS_TABLE')}}</p>


                    <div class="row">
                            <div class="col-md-10">
                                <form action="{{route('super.admins')}}" method="get" class="form-inline">
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-30' : 'mg-r-30' }}">
                                        <input type="text" value="{{ request('name')}}" class="form-control" name="name" placeholder="{{__('users/super_admin.SEARCH_EMPLOYEE_NAME')}}">
                                    </div>
                                    <div class="form-group {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}">
                                        <input type="email" value="{{ request('email')}}" class="form-control" name="email" placeholder="{{__('users/super_admin.SEARCH_EMPLOYEE_EMAIL')}}">
                                    </div>
                                    <div class="form-group ">
                                        <button type="submit" class="btn btn-primary">{{__('users/super_admin.SEARCH_SEARCH_BTN')}}</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2">
                                <a href="{{route('super.admins')}}" class="btn btn-dark">{{__('users/super_admin.SEARCH_CLEAR_SEARCH_BTN')}}</a>
                            </div>
                        </div>
                        <hr>

                    <div class=" rounded table-responsive">
                        <table class="table table-striped table-bordered table-hover mg-b-0">
                            <thead class="thead-colored thead-dark">
                                <tr>
                                    <td>{{__('users/super_admin.TABLE_HEAD_NAME')}}</td>
                                    <td>{{__('users/super_admin.TABLE_HEAD_EMAIL')}}</td>
                                    <td>{{__('users/super_admin.TABLE_HEAD_RANK')}}</td>
                                    <td>{{__('users/super_admin.TABLE_HEAD_DEPT')}}</td>
                                    <td>{{__('users/super_admin.TABLE_HEAD_OPTIONS')}}</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($super_admins as $super_admin)
                                    <tr>
                                        <td class="user-info-table">
                                                <a href="{{route('employee',$super_admin->id)}}">
                                                @if ($super_admin->image == null)
                                                    <img src="{{asset('img/6.jpg')}}" class="user-image rounded-circle" alt="">
                                                @else
                                                    <img src="{{ asset('storage/images/users/'. $super_admin->image) }}" class="user-image rounded-circle" alt="">
                                                @endif
                                                {{$super_admin->name}}
                                                </a>
                                                <div class="user-info" >
                                                       <small><span class="label ">{{__('users/super_admin.TABLE_JOBTITLE')}}</span>  : {{$super_admin->jobTitle->name_en}}</small> 
                                                </div>
                                                <div class="user-info" >
                                                        @if ($super_admin->type->name_en == 'employee')
                                                        <span class="square-8 rounded-circle bg-dark mg-r-10"></span>
                                                        @elseif($super_admin->type->name_en == 'admin')
                                                        <span class="square-8 rounded-circle bg-warning mg-r-10"></span>
                                                        @elseif($super_admin->type->name_en == 'superAdmin')
                                                        <span class="square-8 rounded-circle bg-danger mg-r-10"></span>
                                                        @endif
                                                    <small>{{$super_admin->type->name}} </small>
                                                </div>   
                                        </td>
                                        <td class="user-info-table">
                                                {{$super_admin->email}}
                                        </td>
                                        <td class="user-info-table">
                                                {{$super_admin->rank}}
                                        </td>
                                        <td>{{$super_admin->jobTitle->department->name}}</td>
                                        <td class="options">
                                            @if (Auth::user()->id == $super_admin->id)
                                            <a href="{{route('user.edit',$super_admin->id)}}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @endif               
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $super_admins->appends($_GET)->links() }}
                </div> 
            </div>
        </div>


@endsection






