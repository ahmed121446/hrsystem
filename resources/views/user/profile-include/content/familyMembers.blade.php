<div class=" rounded table-responsive contracts">
        @if (!$employee->socialStatus->is_bread_winner)
        <div class="col-sm-4 col-lg-4">
            <form action="{{route('update.socialStatus',$employee->socialStatus->id)}}" method="post">
                {{csrf_field()}}
                <div class="card shadow-base bd-0">
                    <div class="card-body">  
                            <label class="form-control-label {{ ($errors->has('is_bread_winner')) ? ' is-invalid' : '' }}">
                                {{ __('users/profile.family_BreadWinner')}}:
                            </label>
                            <label class="switch">
                                <input type="checkbox" name="is_bread_winner" >
                                <span class="slider round"></span>
                            </label>              
                    </div><!-- card-body -->
                    <div class="card-footer tx-right">
                        <button type="submit" class="btn btn-primary btn-icon mg-r-5">
                            <div><i class="fa fa-edit"></i></div>
                        </button>
                    </div><!-- card-footer -->
                </div><!-- card -->
            </form>
        </div>
        @else

        
        <a href="{{route('create.familyMembers',$employee->id)}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6' }} tx-12">
                <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
                {{ __('users/profile.family_add')}}
        </a>
        <br>
        @if ($familyMembers->count())
            @foreach($familyMembers->chunk(2) as $chunk)
            <div class="row">
                @foreach ($chunk as $familyMember)
                <div class="col-sm-6 col-lg-6">
                    <div class="card shadow-base bd-0">
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.family_NAME')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$familyMember->name}}</span>
                        </div><!-- card-header -->
                        <div class="card-body">                
                            <p class="tx-12">{{ __('users/profile.family_Email')}}  : {{ $familyMember->email }} </p>
                            <p class="tx-12">{{ __('users/profile.family_National')}}  : {{ $familyMember->national_id }} </p>
                            <p class="tx-12">{{ __('users/profile.family_gender')}} : {{ $familyMember->gender }} </p>
                            <p class="tx-11 mg-b-0 mg-t-15">{{ __('users/profile.family_dob')}} : {{$familyMember->dob}}</p>
                        </div><!-- card-body -->
                        <div class="card-footer tx-right">
                                <a href="{{route('familyMember.edit',$familyMember->id)}}" class="btn btn-primary btn-icon mg-r-5"><div><i class="fa fa-edit"></i></div></a>
                                <a href="#"  data-familymember_id="{{$familyMember->id}}" class="btn btn-danger btn-icon mg-r-5 btnDeleteFamilyMember"><div><i class="fa fa-times-circle"></i></div></a>
                        </div><!-- card-footer -->
                    </div><!-- card -->
                </div>
                @endforeach
            </div>
            @endforeach
            {{ $familyMembers->appends($_GET)->links() }}
        @else
        {{ __('users/profile.family_no')}}
        @endif
    @endif
</div>



