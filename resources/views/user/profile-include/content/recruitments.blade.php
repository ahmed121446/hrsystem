<div class=" rounded table-responsive contracts">
    @if ($recruitment)
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                    <div class="card shadow-base bd-0">
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.recruitment_START_DATE')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$recruitment->start_date}}</span>
                        </div><!-- card-header -->
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0"> {{ __('users/profile.recruitment_END_DATE')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$recruitment->end_date}}</span>
                        </div><!-- card-header -->
                        <div class="card-body">
                            
                            <div class="display-flex">
                                <p> {{ __('users/profile.recruitment_Service_Availability')}}</p>
                                @if ($recruitment->service_availability)
                                <div class="circle circle-success"></div>
                                @else
                                <div class="circle circle-error"></div>
                                @endif
                            </div>     
                            @if ($recruitment->service_availability)                  
                            <div class="display-flex">
                                <p> {{ __('users/profile.recruitment_completed')}}</p>
                                @if ($recruitment->completed)
                                <div class="circle circle-success"></div>
                                @else
                                <div class="circle circle-error"></div>
                                @endif
                            </div> 
                            @endif                      
                        
                        </div><!-- card-body -->
                        <div class="card-footer tx-right">
                            <a href="{{route('recruitment.edit',$recruitment->id)}}" class="btn btn-primary btn-icon mg-r-5"><div><i class="fa fa-edit"></i></div></a>
                        </div><!-- card-footer -->
                    </div><!-- card -->
            </div>
        </div>
    @else
        <a href="{{route('create.recruitment',$employee->id)}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6' }}  tx-12">
                <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
            {{ __('users/profile.recruitment_Add')}}
        </a>
        <br>
        <p>
            {{ __('users/profile.recruitment_no')}}
        </p>
    @endif
   
</div>



