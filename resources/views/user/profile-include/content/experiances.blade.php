<div class=" rounded table-responsive contracts">

        <a href="{{route('create.experiance',$employee->id)}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6' }} tx-12">
                <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
                {{ __('users/profile.salary_Experiences')}}
        </a>
        <br>
    @if ($experiences->count())
    @foreach($experiences->chunk(2) as $chunk)
    <div class="row">
        @foreach ($chunk as $experience)
        <div class="col-sm-6 col-lg-6">
                <div class="card shadow-base bd-0">
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-10 mg-b-0"> {{ __('users/profile.experiance_START_DATE')}}</h6>
                        <span class="tx-12 tx-uppercase">{{$experience->start_date}}</span>
                    </div><!-- card-header -->
                    <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                        <h6 class="card-title tx-uppercase tx-10 mg-b-0"> {{ __('users/profile.experiance_END_DATE')}}</h6>
                        <span class="tx-12 tx-uppercase">{{$experience->end_date}}</span>
                    </div><!-- card-header -->
                  <div class="card-body">
                       
                   
                    <p class="tx-12 label label-danger "> {{ __('users/profile.experiance_where')}} : {{$experience->where}}</p>
                    <p class="tx-12"> {{ __('users/profile.experiance_Role')}} : {{ $experience->role }} </p>
                    <p class="tx-11 mg-b-0 mg-t-1"> {{ __('users/profile.experiance_Description')}} : {{ $experience->description }} </p>
                  </div><!-- card-body -->
                  <div class="card-footer tx-right">
                        <a href="{{route('experiance.edit',$experience->id)}}" class="btn btn-primary btn-icon mg-r-5"><div><i class="fa fa-edit"></i></div></a>
                        <a href="#" data-experiances_id="{{$experience->id}}" class="btn btn-danger btn-icon mg-r-5 btnDeleteExperiance"><div><i class="fa fa-times-circle"></i></div></a>
                    </div><!-- card-footer -->
                </div><!-- card -->
        </div>
        @endforeach
    </div>
    @endforeach
    @else
    {{ __('users/profile.experiance_no_Description')}}
    @endif
    {{ $experiences->appends($_GET)->links() }}
</div>



