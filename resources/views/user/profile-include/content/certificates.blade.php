<div class=" rounded table-responsive contracts">
        <a href="{{route('create.certificate',$employee->id)}}" class=" mg-b-25 btn btn-danger pd-x-30 pd-y-15 tx-uppercase tx-bold {{(app()->getLocale() == 'ar') ? 'tx-spacing-1' : 'tx-spacing-6' }} tx-12">
                <i class="fa fa-plus {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10' }}"></i>
                {{ __('users/profile.certificate_Add')}}
        </a>
        <br>
    @if ($certificates->count())
        @foreach($certificates->chunk(2) as $chunk)
        <div class="row">
            @foreach ($chunk as $certificate)
            <div class="col-sm-6 col-lg-6">
                    <div class="card shadow-base bd-0">
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase {{(app()->getLocale() == 'ar') ? 'tx-12' : 'tx-10' }} mg-b-0">
                                <span class="label label-danger"> {{$certificate->name}} </span>
                            </h6>
                        </div><!-- card-header -->
                        <div class="card-body">   
                            @if ($certificate->location)  
                            <p class="tx-12">{{ __('users/profile.certificate_Location')}} : {{ $certificate->location }} </p>
                            @endif             
                            <p class="tx-12">{{ __('users/profile.certificate_Date')}} : {{ $certificate->date }} </p>
                        </div><!-- card-body -->
                        <div class="card-footer tx-right">
                                <a href="{{route('certificate.edit',$certificate->id)}}" class="btn btn-primary btn-icon mg-r-5"><div><i class="fa fa-edit"></i></div></a>
                                <a href="#" data-certificate_id="{{$certificate->id}}"  class="btn btn-danger btn-icon mg-r-5 btnDeleteCertificate"><div><i class="fa fa-times-circle"></i></div></a>
                        </div><!-- card-footer -->
                    </div><!-- card -->
            </div>
            @endforeach
        </div>
        @endforeach
        @else
        {{ __('users/profile.certificate_no_Certificates')}}
        @endif
        {{ $certificates->appends($_GET)->links() }}
    </div>
    
    
    
    