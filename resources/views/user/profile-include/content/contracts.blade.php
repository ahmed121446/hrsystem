<div class=" rounded table-responsive contracts">
    @if ($contracts->count())
        @foreach($contracts->chunk(2) as $chunk)
        <div class="row">
            @foreach ($chunk as $contract)
            <div class="col-sm-6 col-lg-6">
                    <div class="card shadow-base bd-0">
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.contract_id')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$contract->id}}</span>
                        </div><!-- card-header -->
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.contract_START_DATE')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$contract->start_date}}</span>
                        </div><!-- card-header -->
                        <div class="card-header bg-transparent d-flex justify-content-between align-items-center">
                            <h6 class="card-title tx-uppercase tx-10 mg-b-0">{{ __('users/profile.contract_END_DATE')}}</h6>
                            <span class="tx-12 tx-uppercase">{{$contract->end_date}}</span>
                        </div><!-- card-header -->
                        <div class="card-body">
                            
                            <div class="display-flex">
                                <p>{{ __('users/profile.contract_Active')}}</p>
                                @if ($contract->is_active)
                                <div class="circle circle-success"></div>
                                @else
                                <div class="circle circle-error"></div>
                                @endif
                            </div>                       
                            <p class="tx-12">{{ __('users/profile.contract_Monthly_Salary')}} : {{ number_format($contract->monthly_salary) }} {{$contract->currancy}}</p>
                            <p class="tx-12">{{ __('users/profile.contract_Annual_Salary')}} : {{ number_format($contract->total_annual,2) }} {{$contract->currancy}}</p>
                            
                        </div><!-- card-body -->
                        <div class="card-footer tx-right">
                            <a href="{{route('contract.edit',$contract->id)}}" class="btn btn-primary btn-icon mg-r-5">
                                <div><i class="fa fa-edit"></i></div>
                            </a>
                            <a href="#" data-contract_id="{{$contract->id}}" class="btn btn-danger btn-icon mg-r-5 btnDeleteContract">
                                <div><i class="fa fa-times-circle"></i></div>
                            </a>
                        </div><!-- card-footer -->
                    </div><!-- card -->
            </div>
            @endforeach
        </div>
        @endforeach
        {{ $contracts->appends($_GET)->links() }}
    @else
    {{ __('users/profile.contract_no_contract')}}
    @endif

</div>



