<!-- users -->
<div class="col-sm-6 col-xl-4 statistics">
  <div class="bg-danger rounded overflow-hidden">
    <div class="pd-25 d-flex align-items-center">
      <i class="fa fa-users tx-60 lh-0 tx-white op-7"></i>
      <div class="mg-l-20 mg-r-20">
        <p class="{{(app()->getLocale() == 'ar') ? 'tx-18' : 'tx-10' }}   tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">{{__('dashboard.users')}}</p>
        <p class="tx-10 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$users}}</p>
      </div>
    </div>
  </div>
</div>



<!-- departments -->
<div class="col-sm-6 col-xl-4 statistics">
  <div class="bg-primary rounded overflow-hidden">
    <div class="pd-25 d-flex align-items-center">
      <i class="fa fa-list-ul tx-60 lh-0 tx-white op-7"></i>
      <div class="mg-l-20 mg-r-20">
        <p class="{{(app()->getLocale() == 'ar') ? 'tx-18' : 'tx-10' }} tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">{{__('dashboard.DEPARTMENTS')}}</p>
        <p class="tx-10 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$departments}}</p>
      </div>
    </div>
  </div>
</div>


<!-- job titles -->
<div class="col-sm-6 col-xl-4 statistics">
  <div class="bg-br-primary rounded overflow-hidden">
    <div class="pd-25 d-flex align-items-center">
      <i class="fa fa-tty tx-60 lh-0 tx-white op-7"></i>
      <div class="mg-l-20 mg-r-20">
        <p class="{{(app()->getLocale() == 'ar') ? 'tx-18' : 'tx-10' }} tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">{{__('dashboard.jobtitles')}}</p>
        <p class="tx-10 tx-white tx-lato tx-bold mg-b-2 lh-1">{{$jobTitles}}</p>
      </div>
    </div>
  </div>
</div>