<div id="add-skill-modal" class="modal fade">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Skill</h6>
                <button type="button" id="close-skill-modal" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
           
            <form action="{{ route('skill.create') }}" method="post">
                {{csrf_field()}}
                <div class="modal-body pd-15">
                        <div class="form-layout form-layout-6">
                            <div class="row no-gutters">
                                <div class="col-5 col-sm-4">
                                    Name :
                                </div>
                                <!-- col-4 -->
                                <div class="col-7 col-sm-8">
                                    <input class="form-control" type="text" name="name_en" value="{{old('name_en')}}" placeholder="Skill Name">
                                </div>
                                <!-- col-8 -->
                            </div>
                            <!-- row -->
                            <div class="row no-gutters">
                                <div class="col-5 col-sm-4">
                                    اسم:
                                </div>
                                <!-- col-4 -->
                                <div class="col-7 col-sm-8">
                                    <input class="form-control" type="text" value="{{old('name_ar')}}" name="name_ar" placeholder="اسم المهارة">
                                </div>
                                <!-- col-8 -->
                            </div>
                            <!-- row -->
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Create</button>
                    <button type="button" id="btn-close-skill-modal" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium"
                        data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>