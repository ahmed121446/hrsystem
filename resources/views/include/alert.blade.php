@if ($message  = session('message') )
    @if ($type  = session('type') )
    <div class="alert alert-{{$type}} alert-bordered pd-y-10" style="opacity: 1;" role="alert">
    @endif
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="d-flex align-items-center justify-content-start">
            @if ($type  == 'danger' )
                <i class="icon ion-ios-close alert-icon tx-24 tx-danger {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
            @elseif($type  == 'success' )
                <i class="icon ion-ios-checkmark alert-icon tx-24 tx-success {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
            @elseif($type  == 'warning' )
            <i class="icon ion-alert-circled alert-icon tx-24 tx-warning {{(app()->getLocale() == 'ar') ? 'mg-l-10' : 'mg-r-10 ' }} "></i>
            @endif
            <div>
                @if ($type  == 'danger' )
                    <p class="mg-b-2 tx-danger">{{$message}}.</p>
                @elseif($type  == 'success' )
                    <p class="mg-b-2 tx-success">{{$message}}.</p>
                @elseif($type  == 'warning' )
                    <p class="mg-b-2 tx-warning">{{$message}}.</p>
                @endif
                @if ($sub_message  = session('sub-message'))
                    <p class="mg-b-0 tx-gray">{{$sub_message}}</p>
                @endif
            </div>
        </div>
        <!-- d-flex -->
    </div>
    <!-- alert -->


    <script>        
        // setTimeout(function() {
        //     $('.alert').fadeOut('fast');
        // }, 4000);
    </script>
@endif