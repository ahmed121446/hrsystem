<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Change Password Page',
    'SUB_TITLE' => 'CHANGE PASSWORD',

    'FORM_OLD' => ' OLD PASSWORD',
    'FORM_NEW' => ' NEW PASSWORD',
    'FORM_CONFIRM' => 'CONFIRM NEW PASSWORD',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
 
    
];
