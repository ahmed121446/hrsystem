<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'header' => 'Employee Page',

    // left part
    'Form_Information' => 'Information',
    'Form_changepass' => 'Change Password',
    'Form_NAME' => 'NAME ',
    'Form_mobile' => 'PHONE NUMBER ',
    'Form_EMAIL_ADDRESS' => 'EMAIL ADDRESS',
    'Form_NATIONALID' => 'NATIONAL ID',
    'Form_COUNTRY' => 'COUNTRY',
    'Form_CITY' => 'CITY',
    'Form_TYPE' => 'TYPE',
    'Form_OTHER_INFORMATION' => 'OTHER INFORMATION',
    'Form_DEPARTMENT' => 'DEPARTMENT',
    'Form_HIRE' => 'HIRE DATE',
    'Form_JOB' => 'JOB Title',
    'Form_RANK' => 'RANK ',
    'Form_EXPERIANCE_YEARS' => 'EXPERIANCE YEARS ',
    'Form_INSURANCE_NUMBER' => 'INSURANCE NUMBER ',
    'Form_SOCIAL_STATUS' => 'SOCIAL STATUS  ',
    'Form_ADDSKILL' => 'Add Skill ',
    'Form_Bread_Winner' => 'Bread Winner ',
    'FORM_rank1' =>'Team Leader',
    'FORM_rank2' =>'Senior',
    'FORM_rank3' =>'Middle',
    'FORM_rank4' =>'Junior',


    //right part
    'title_contracts'=>'Contracts',
    'title_Salaries'=>'Salaries',
    'title_Experiances'=>'Experiances',
    'title_Certificates'=>'Certificates',
    'title_Recruitment'=>'Recruitment',
    'title_Family_Members'=>'Family Members',


    'contract_id'=>'ID',
    'contract_START_DATE'=>'START DATE',
    'contract_END_DATE'=>'END DATE',
    'contract_Active'=>'Active',
    'contract_Monthly_Salary'=>'Monthly Salary',
    'contract_Annual_Salary'=>'Annual Salary',
    'contract_no_contract'=>'No Contracts',


    'salary_create'=>'Create Salaries',
    'salary_create_contract'=>'Create Active Contract',
    'salary_id'=>'ID',
    'salary_CONTRACT_ID' =>'CONTRACT ID',
    'salary_DATE' =>'DATE',
    'salary_INCENTIVE' =>' INCENTIVE',
    'salary_DEDUCTION' =>' DEDUCTION',
    'salary_OTHER' =>'OTHER',
    'salary_ALLOWANCE' =>'ALLOWANCE',
    'salary_TAX' =>'TAX',
    'salary_LOAN' =>'LOAN',
    'salary_net' =>'NET SALARY',
    'salary_total' =>'TOTAL',


    'salary_Experiences'=>'Create Experience',
    'experiance_START_DATE'=>'START DATE',
    'experiance_END_DATE'=>'END DATE',
    'experiance_where'=>'Where',
    'experiance_Role'=>'Role',
    'experiance_Description'=>'Description',
    'experiance_no_Description'=>'No Experiences',


    'certificate_Add'=>'Add Certificate',
    'certificate_Location'=>'Location',
    'certificate_Date'=>'Date',
    'certificate_no_Certificates'=>'No Certificates',


    'recruitment_START_DATE'=>'START DATE',
    'recruitment_END_DATE'=>'END DATE',
    'recruitment_Service_Availability'=>'Service Availability',
    'recruitment_completed'=>'Completed',
    'recruitment_Add'=>'Add Recruitment',
    'recruitment_no'=>'No Recruitment',
    
    
    'family_BreadWinner'=>'BreadWinner',
    'family_add'=>'Add Family Member',
    'family_no'=>'No Family Members',
    'family_NAME' => 'NAME ',
    'family_Email' => 'Email ',
    'family_National' => 'National ID ',
    'family_gender' => 'Gender ',
    'family_dob' => 'Date Of Birth  ',

    
];
