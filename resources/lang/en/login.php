<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'head' => 'HR - System',
    'title' => 'Login to your account.',
    'forget_password' => 'Forgot password?',
    'btn_login' => 'LogIn',
    'email_placeholder' => 'Enter your email',
    'password_placeholder' => 'Enter your password',

];
