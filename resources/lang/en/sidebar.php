<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'nav' => 'NAVIGATION',
    'dashboard' => 'Dashboard',
    'users' => ' Users',
    'all' => ' All',
    'employees' => 'Employees',
    'admins' => 'Admins',
    'super_admins' => 'Super-Admins',
    'salaries' => 'Salaries',
    'contracts' => 'Contracts',
    'skills' => 'Skills',
    'departments' => 'Departments',
    'job_titles' => 'Job Titles',
    'team' => 'My Team',

];
