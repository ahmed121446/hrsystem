<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Edit Recruitment Page',
    'SUB_TITLE' => 'Edit Recruitment',

    'FORM_EMPLOYEE_NAME' =>'EMPLOYEE NAME',
    'FORM_START_DATE'=>' START DATE',
    'FORM_END_DATE'=>' END DATE',
    'FORM_Service_Availability'=>' Availability ',
    'FORM_completed'=>' Completed',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
