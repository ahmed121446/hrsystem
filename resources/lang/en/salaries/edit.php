<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Edit Salary Page',
    'CONTRACT_TABLE' => 'Edit Salary',

    'FORM_Employee' =>'Employee Name',
    'FORM_MONTHLY_SALARY' =>'Monthly Salary',
    'FORM_CONTRACT_ID' =>'contract ID',
    'FORM_DATE' =>'DATE',
    'FORM_INCENTIVE' =>' INCENTIVE',
    'FORM_DEDUCTION' =>' DEDUCTION',
    'FORM_OTHER' =>'OTHER',
    'FORM_ALLOWANCE' =>'ALLOWANCE',
    'FORM_TAX' =>'TAX',
    'FORM_LOAN' =>'LOAN',

    


    'FORM_PLACEHOLDER' =>'0.00',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
