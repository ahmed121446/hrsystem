<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Skills Page',
    'SKILL_TABLE' => 'Skills ',
    'SUB_TITLE_SKILL_TABLE' => 'All skills in system.',

    'CREAT_BUTTON' => 'Create Skills',



    // 'SEARCH_EMPLOYEE_NAME' => 'Employee name',
    // 'SEARCH_EMPLOYEE_EMAIL' => 'Employee email',
    // 'SEARCH_EMPLOYEE_NATIONAL_ID' => 'Employee national id',
    // 'SEARCH_SUPER_NAME' => 'Supervisor name',
     'SEARCH_DEPT_NAME' => 'Department name',
    // 'SEARCH_JOB_NAME' => 'Job title name',
    'SEARCH_FOR_NAME' => 'Search By Name',
    'SEARCH_SEARCH_BTN' => 'Search',
    'SEARCH_CLEAR_SEARCH_BTN' => 'Clear Search',
    
    
    
    'TABLE_HEAD_NAME' => 'Name',
    // 'TABLE_HEAD_SUPER' => 'Supervisor',
    'TABLE_HEAD_DEPT' => 'Department',
    'TABLE_HEAD_OPTIONS' => 'Options',

    'TABLE_JOBTITLE' => 'Job Title',
    'TABLE_Email' => 'Email',
    'TABLE_EMPLOYEES' => 'Employee',
    
];
