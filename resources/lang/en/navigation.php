<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo' => 'HR-System',
    'notification' => 'Notifications',
    'MarkRead' => 'Mark All as Read',
    
    'viewprofile' => 'View Profile',
    'editprofile' => 'Update Profile',
    'logout' => 'Log Out',
];
