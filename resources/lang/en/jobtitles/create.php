<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Create Job Title Page',
    'JOB_TABLE' => 'Create Job Title',
    'SUB_TITLE_JOB_TABLE' => 'New job title will be added to system.',

    'CREAT_BUTTON' => 'Create Job Title',

    'FORM_NAME_EN' =>'Name in english ',
    'FORM_NAME_AR' =>'Name in arabic',
    'FORM_DEPT' =>'Department',


    'FORM_NAME_EN_PLACEHOLDER' =>'Enter name in english ',
    'FORM_NAME_AR_PLACEHOLDER' =>'Enter name in arabic ',
    'FORM_DEPT_PLACEHOLDER' =>'Choose department',


    'CREAT_CANCEL' => 'Cancel',
    'CREAT_BUTTON' => 'Submit Data',
    
];
