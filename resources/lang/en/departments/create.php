<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Create Department Page',
    'JOB_TABLE' => 'Create Department',
    'SUB_TITLE_JOB_TABLE' => 'New Department will be added to system.',

    'CREAT_BUTTON' => 'Create Department',

    'FORM_NAME_EN' =>'Name in english ',
    'FORM_Description_EN' =>'Description in english ',
    'FORM_NAME_AR' =>'Name in arabic',
    'FORM_Description_AR' =>'Description in arabic',

    'FORM_NAME_EN_PLACEHOLDER' =>'Enter name in english ',
    'FORM_DESC_EN_PLACEHOLDER' =>'Enter description in english ',
    'FORM_NAME_AR_PLACEHOLDER' =>'Enter name in arabic ',
    'FORM_DESC_AR_PLACEHOLDER' =>'Enter description in arabic ',



    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
