<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'Add Experience Page',
    'SUB_TITLE' => 'Add Experience',
    'MINI'=>'New experiance will be added to system',

    'FORM_EMPLOYEE_NAME' =>'EMPLOYEE NAME',
    'FORM_STARTDATE' =>'START DATE',
    'FORM_END_DATE' =>'END DATE',
    'FORM_WHERE' =>'WHERE',
    'FORM_ROLE_ARABIC' =>'ROLE IN ARABIC',
    'FORM_ROLE_ENGLISH' =>'ROLE IN ENGLISH',
    'FORM_DESCRIPTION_IN_ARABIC' =>'DESCRIPTION IN ARABIC',
    'FORM_DESCRIPTION_IN_ENGLISH' =>'DESCRIPTION IN ENGLISH',

    'FORM_WHERE_PLACEHOLDER' =>'ENTER WHERE',
    'FORM_ROLE_AR_PLACEHOLDER' =>'ENTER ROLE IN ARABIC',
    'FORM_ROLE_EN_PLACEHOLDER' =>'ENTER ROLE IN ENGLISH',
    'FORM_desc_AR_PLACEHOLDER' =>'ENTER DESCRIPTION IN ARABIC',
    'FORM_desc_EN_PLACEHOLDER' =>'ENTER DESCRIPTION IN ENGLISH',

    'CREAT_BUTTON' => 'Submit Data',
    'CREAT_CANCEL' => 'Cancel',
    
];
