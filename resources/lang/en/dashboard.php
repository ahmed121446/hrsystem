<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Dashboard' => 'Dashboard',
    'users' => 'USERS',
    'DEPARTMENTS' => 'DEPARTMENTS',
    'jobtitles' => 'JOB TITLES',

    'MONTHLY_TOTAL_SALARIES' => 'MONTHLY TOTAL SALARIES',
    'MONTHLY_DEDUCTION' => 'MONTHLY DEDUCTION',
    'MONTHLY_INSENTIVE' => 'MONTHLY INSENTIVE',


    'TOP_MONTHLY_INSENTIVE' => 'TOP MONTHLY INSENTIVE',
    'Department' => 'Department',
    'jobtitle' => 'Job Title',
    'Employee_Name' => 'Employee Name',
    'INSENTIVE' => 'INSENTIVE',
    'Total' => 'Total',
    


];
