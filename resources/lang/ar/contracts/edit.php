<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'الصفحة تحديث عقود',
    'CONTRACT_TABLE' => 'تحديث عقد',

    'FORM_USER' =>'المستعمل',
    'FORM_MONTHLY_SALARY' =>'راتب شهري',
    'FORM_CURRANCY' =>'العملة',
    'FORM_START_DATE' =>'تاريخ البدء',
    'FORM_END_DATE' =>'تاريخ الانتهاء',
    'FORM_ACTIVE' =>'نشط',


    'FORM_MONTHLY_SALARY_PLACEHOLDER' =>'أدخل الراتب الشهري',
    'FORM_CURRANCY_PLACEHOLDER' =>'اختر العملة',

    'CREAT_CANCEL' => 'إلغاء',
    'CREAT_BUTTON' => 'تقديم البيانات',

];
