<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'nav' => 'قائمة التنقل',
    'dashboard' => 'لوحة القيادة',
    'users' => ' المستخدمين',
    'all' => ' الكل',
    'employees' => 'الموظفين',
    'admins' => 'مشرف',
    'super_admins' => 'مشرف متميز',
    'salaries' => 'رواتب',
    'contracts' => 'عقود',
    'skills' => 'مهارات',
    'departments' => 'الإدارات',
    'job_titles' => 'الوظائف',
    'team' => 'فريقي',
];
