<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo' => 'نظام-الموارد البشرية',
    'notification' => 'إشعارات',
    'MarkRead' => 'اجعل كل شيء مقروءًا',

    'viewprofile' => 'عرض الصفحة الشخصية',
    'editprofile' => 'تحديث الملف',
    'logout' => 'الخروج',
];
