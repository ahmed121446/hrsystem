<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => ' صفحة تحديث عنوان الوظيفة',
    'JOB_TABLE' => 'تحديث عنوان الوظيفة',

    'FORM_NAME_EN' =>'الاسم باللغة العربية',
    'FORM_NAME_AR' =>'الاسم باللغة الانجليزية',
    'FORM_DEPT' =>'إدارة',


    'FORM_NAME_EN_PLACEHOLDER' =>'أدخل الاسم باللغة الإنجليزية',
    'FORM_NAME_AR_PLACEHOLDER' =>'أدخل الاسم باللغة العربية',
    'FORM_DEPT_PLACEHOLDER' =>'اختر القسم',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
