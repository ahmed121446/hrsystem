<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'Dashboard' => 'لوحة القيادة',
    'users' => 'المستخدمين',
    'DEPARTMENTS' => 'الإدارات',
    'jobtitles' => 'وظائف',

    
    'MONTHLY_TOTAL_SALARIES' => 'مجموع المرتبات الشهرية',
    'MONTHLY_DEDUCTION' => ' مجموع الخصومات الشهرية',
    'MONTHLY_INSENTIVE' => 'مجموع الحوافز الشهرية ',

    'TOP_MONTHLY_INSENTIVE' => 'الموظفين الاكثر حوافز في الشهر',
    'Department' => 'إدارة',
    'jobtitle' => 'المسمى الوظيفي',
    'Employee_Name' => 'اسم الموظف',
    'INSENTIVE' => 'حافز',
    'Total' => 'مجموع',


];
