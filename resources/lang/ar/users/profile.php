<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة الموظف',

    // left part
    'Form_Information' => 'معلومات',
    'Form_changepass' => 'تغيير كلمة السر',
    'Form_NAME' => 'اسم ',
    'Form_mobile' => 'رقم الهاتف ',
    'Form_EMAIL_ADDRESS' => 'البريد الإلكتروني',
    'Form_NATIONALID' => 'الهوية الوطنية',
    'Form_COUNTRY' => 'بلد',
    'Form_CITY' => 'مدينة',
    'Form_TYPE' => 'نوع الموظف',
    'Form_OTHER_INFORMATION' => 'معلومات أخرى',
    'Form_DEPARTMENT' => 'إدارة',
    'Form_HIRE' => 'تاريخ توظيف',
    'Form_JOB' => 'المسمى الوظيفي',
    'Form_RANK' => 'مرتبة ',
    'Form_EXPERIANCE_YEARS' => 'سنوات الخبرة ',
    'Form_INSURANCE_NUMBER' => 'رقم التأمين ',
    'Form_SOCIAL_STATUS' => 'الحالة الاجتماعية  ',
    'Form_ADDSKILL' => 'أضف مهارة ',
    'Form_Bread_Winner' => 'عَائِلُ الْأُسْرَةِ ',
    'FORM_rank1' =>'رئيس الفريق ',
    'FORM_rank2' =>'كبير',
    'FORM_rank3' =>'وسط',
    'FORM_rank4' =>'الأصغر',



    //right part
    'title_contracts'=>'عقود',
    'title_Salaries'=>'الرواتب',
    'title_Experiances'=>'خبرات',
    'title_Certificates'=>'شهادات',
    'title_Recruitment'=>'تجنيد',
    'title_Family_Members'=>'أفراد الأسرة',


    'contract_id'=>'الهوية',
    'contract_START_DATE'=>'تاريخ البدء',
    'contract_END_DATE'=>'تاريخ الانتهاء',
    'contract_Active'=>'نشيط',
    'contract_Monthly_Salary'=>'راتب شهري',
    'contract_Annual_Salary'=>'الراتب السنوي',
    'contract_no_contract'=>'لا توجد عقود',


    'salary_create'=>'إضافة راتب',
    'salary_create_contract'=>'إنشاء عقد نشط',
    'salary_id'=>'الهوية',
    'salary_CONTRACT_ID' =>'معرف العقد',
    'salary_DATE' =>'التاريخ',
    'FORM_MONTHLY_SALARY' =>'راتب شهري',
    'salary_INCENTIVE' =>'حافز',
    'salary_DEDUCTION' =>'المستقطع',
    'salary_OTHER' =>'بدل',
    'salary_ALLOWANCE' =>'آخر',
    'salary_TAX' =>'ضريبة',
    'salary_LOAN' =>'قرض',
    'salary_net' =>'صافي الراتب',
    'salary_total' =>'مجموع الراتب',

    
    'salary_Experiences'=>'إضافة خبرة',
    'experiance_START_DATE'=>'تاريخ البدء',
    'experiance_END_DATE'=>'تاريخ الانتهاء',
    'experiance_where'=>'أين',
    'experiance_Role'=>'دور',
    'experiance_Description'=>'وصف',
    'experiance_no_Description'=>'لا تجارب',


    'certificate_Add'=>'إضافة شهادة',
    'certificate_Location'=>'الموقع',
    'certificate_Date'=>'التاريخ',
    'certificate_no_Certificates'=>'لم يتم العثور على شهادات',


    'recruitment_START_DATE'=>'تاريخ البدء',
    'recruitment_END_DATE'=>'تاريخ الانتهاء',
    'recruitment_Service_Availability'=>' توفر الخدمة',
    'recruitment_completed'=>'تم الانتهاء',
    'recruitment_Add'=>'أضف التوظيف العسكري',
    'recruitment_no'=>'لا توجد بيانات للتجنيد العسكري',


    'family_BreadWinner'=>'عائل الأسرة',
    'family_add'=>'إضافة عضو للعائلة',
    'family_no'=>'لا يوجد أفراد الأسرة',
    'family_NAME' => 'اسم ',
    'family_Email' => 'البريد الإلكتروني ',
    'family_National' => ' الهوية الوطنية ',
    'family_gender' => 'جنس ',
    'family_dob' => 'تاريخ الولادة',





];
