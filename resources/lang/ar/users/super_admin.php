<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة المشرفون المميزون',
    'USERS_TABLE' => 'قائمة المشرفون المميزون',
    'SUB_TITLE_USERS_TABLE' => 'جميع المشرفون المميزون في النظام.',



    'SEARCH_EMPLOYEE_NAME' => 'اسم الموظف',
    'SEARCH_EMPLOYEE_EMAIL' => 'البريد الإلكتروني للموظف',
    'SEARCH_EMPLOYEE_NATIONAL_ID' => 'الهوية الوطنية للموظف',
    'SEARCH_SUPER_NAME' => 'اسم المشرف',
    'SEARCH_DEPT_NAME' => 'اسم القسم',
    'SEARCH_JOB_NAME' => 'اسم الوظيفة',
    'SEARCH_SEARCH_BTN' => 'بحث',
    'SEARCH_CLEAR_SEARCH_BTN' => 'مسح البحث',


    'TABLE_HEAD_NAME' => 'اسم',
    'TABLE_HEAD_EMAIL' => 'البريد الإلكتروني',
    'TABLE_HEAD_RANK' => 'ترتيبه',
    'TABLE_HEAD_DEPT' => 'إدارة',
    'TABLE_HEAD_OPTIONS' => 'خيارات',

    'TABLE_JOBTITLE' => 'المسمى الوظيفي',
    'TABLE_Email' => 'البريد الإلكتروني',

];
