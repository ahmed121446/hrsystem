<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' =>'صفحة إضافة مهارة للمستخدم',
    'SUB_TITLE' => 'إضافة مهارة للمستخدم',

    'FORM_NAME' =>'اسم الموظف',
    'FORM_SKILL' =>'مهارة',
    'FORM_LEVEL' =>'مستوى',
    'FORM_DATE' =>'تاريخ الاكتساب',
    
    'FORM_DEPT_PLACEHOLDER' =>'Choose مهارة',
    'FORM_level_PLACEHOLDER' =>'Choose مستوى',

    'CREAT_BUTTON' => 'تقديم البيانات',
    'CREAT_CANCEL' => 'إلغاء',

];
