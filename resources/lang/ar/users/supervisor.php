<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة فريق المشرف',
    'USERS_TABLE' => 'جدول فريق المشرف',
    'SUB_TITLE_USERS_TABLE' => 'جميع فريق المشرف في النظام.',
    'SUB_TITLE_USERS_TABLE1' => 'كل فريق ',
    'SUB_TITLE_USERS_TABLE2' => '.',


    'TABLE_HEAD_NAME' => 'اسم',
    'TABLE_HEAD_EMAIL' => 'البريد الإلكتروني',
    'TABLE_HEAD_RANK' => 'ترتيبه',
    'TABLE_HEAD_SUPER' => 'مشرف',
    'TABLE_HEAD_DEPT' => 'إدارة',
    'TABLE_HEAD_OPTIONS' => 'خيارات',

    'TABLE_JOBTITLE' => 'المسمى الوظيفي',
    'TABLE_Email' => 'البريد الإلكتروني',

];
