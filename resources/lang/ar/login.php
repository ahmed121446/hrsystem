<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'head' => ' الموارد البشرية',
    'title' => 'تسجيل الدخول إلى حسابك.',
    'forget_password' => 'هل نسيت كلمة المرور؟',
    'btn_login' => 'تسجيل الدخول',
    'email_placeholder' => 'أدخل بريدك الالكتروني',
    'password_placeholder' => 'ادخل رقمك السري',

];
