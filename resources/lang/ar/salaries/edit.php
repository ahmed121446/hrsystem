<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'header' => 'صفحة تحديث الراتب',
    'CONTRACT_TABLE' => 'تحديث الراتب',

    'FORM_Employee' =>'المستعمل',
    'FORM_CONTRACT' =>'راتب شهري',
    'FORM_CONTRACT_ID' =>'معرف العقد',
    'FORM_DATE' =>'التاريخ',
    'FORM_MONTHLY_SALARY' =>'راتب شهري',
    'FORM_INCENTIVE' =>'حافز',
    'FORM_DEDUCTION' =>'المستقطع',
    'FORM_OTHER' =>'بدل',
    'FORM_ALLOWANCE' =>'آخر',
    'FORM_TAX' =>'ضريبة',
    'FORM_LOAN' =>'قرض',


    'FORM_PLACEHOLDER' =>'0.00',

    'CREAT_CANCEL' => 'إلغاء',
    'CREAT_BUTTON' => 'تقديم البيانات',

];
